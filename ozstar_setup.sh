#/bin/bash

HERE=/fred/oz005/users/mlower/double_psr/double_psr_eclipses

export MLOWER=/fred/oz002/mlower
export PSRHOME=/fred/oz002/psrhome
source ${PSRHOME}/scripts/psrhome.sh
#export TEMPO2=/fred/oz002/rshannon/tempo2

# Unload psrhome Python modules
module unload anaconda2/5.1.0
module load python/3.6.4
module load numpy/1.16.3-python-3.6.4
module load scipy/1.3.0-python-3.6.4
module load psrchive/b7ad99c37-python-3.6.4
module load astropy/3.1.2-python-3.6.4
module load mpi4py/3.0.0-python-3.6.4

export LD_LIBRARY_PATH=${MLOWER}/src/MultiNest/lib:$LD_LIBRARY_PATH

# Recompile Eclipse0737
#python setup.py install --user
