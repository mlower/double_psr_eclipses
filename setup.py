from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext
from Cython.Build import cythonize
import numpy as np

ext_modules = [
    Extension("Eclipse0737.Eclipse",
        sources=["Eclipse0737/Eclipse.pyx"],
        libraries=["m"],
        include_dirs=[np.get_include()])
    ]

install_requires = [
    'numpy',
    'astropy'
    ]

setup(
    name = 'Eclipse0737',
    author='Rene Breton',
    author_email='rene.breton@manchester.ac.uk',
    packages=['Eclipse0737'],
    install_requires = install_requires,
    #cmdclass = {'build_ext': build_ext},
    ext_modules = cythonize(ext_modules)
    )

