import numpy as np
from astropy.time import Time
import matplotlib.pyplot as plt
import time

import Eclipse0737

start = time.time()

SECPERDAY = 24*60*60
DEGTORAD = np.pi/180

porb = 0.10225156246204 * SECPERDAY
p0 = 1/0.36056035506
delta_porbp0 = porb / p0

n = 1001
orbph = np.linspace(-1.0, 1.0, n)/360 + 0.25
phsB = (orbph - orbph[0]) * delta_porbp0
fluxes = np.zeros_like(phsB)
err = np.ones_like(phsB)
eclipse = Eclipse0737.EclipseDipole(phsB, orbph, fluxes, err)

m = eclipse.Model()

Phi = lambda MJD: (51.21 - 5.0734*(MJD-53857)/365.25)

par = {
    'theta': DEGTORAD*130.02,
    'phi': DEGTORAD*Phi(Time('2020-06-25T00:00:00Z').mjd),
    'chi': DEGTORAD*70.92,
    'nu': 0.8,
    'rmin': 0.35,
    'upsilon': 2.0,
    'z': -0.5,
    'ratio': 1.,
    'shift_phsB': 0.,
    'do_phase': 1.
    }

#par['phi'] = DEGTORAD*Phi(Time('2019-03-26T00:00:00Z').mjd)
m = eclipse.Model(**par)

finish = time.time()
print(finish - start)

quit()

plt.figure()
plt.plot(eclipse.orbph, m)
plt.title('2020-06-25')
plt.savefig('eclipse_2020-06-25.pdf')

quit()

par['phi'] = DEGTORAD*Phi(Time('2021-03-26T00:00:00Z').mjd)
m = eclipse.Model(**par)
plt.figure()
plt.plot(eclipse.orbph, m)
plt.title('2021-03-26')
plt.savefig('eclipse_2021-03-26.pdf')

par['phi'] = DEGTORAD*Phi(Time('2009-03-26T00:00:00Z').mjd)
m = eclipse.Model(**par)
plt.figure()
plt.plot(eclipse.orbph, m)
plt.title('2021-03-26')
