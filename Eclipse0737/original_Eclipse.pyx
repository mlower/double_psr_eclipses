from __future__ import print_function, division
import numpy as np

cimport cython
from cpython cimport bool
from libc.math cimport sin, cos, atan2, sqrt, pow, exp, abs, M_PI

ctypedef fused my_type:
    int
    double
    long



# import numpy as np
# cimport numpy as np
# cimport cython


# cdef extern from "math.h":
#     double sin(double)
#     double cos(double)
#     double atan2(double, double)
#     double sqrt(double)
#     double pow(double, double)
#     double exp(double)
#     double abs(double)
#     double M_PI



cdef class EclipseDipole:
    """EclipseDipole(phsB, orbph, flux=None, err=None)
    EclipseDipole class.

    Parameters
    ----------
    phsB : ndarray
        Spin phase of B corresponding to each point in the lightcurve.
    orbph : ndarray
        Orbital phase corresponding to each point in the lightcurve.
    flux : ndarray, None
        Flux for each point in the lightcurve.
        If not provided, the array will be filled with zeros, which can be
        useful for testing and theoretical lightcurve calculations.
    err : ndarray, None
        Error for each point in the lightcurve.
        If not provided, the array will be filled with ones, which can be
        useful for testing and theoretical lightcurve calculations.

    Examples
    --------
    >>> myeclipse = EclipseDipole(phsB, orbph, flux, err)
    """
    ##-------------------------------------------------------------------------
    ## Some class variables
    cdef int n                              ## number of data points

    cdef public double  Bmax0               ## maximum B-field at 1 GHz

    cdef public double rcool                ## cooling radius (defined in the constructor)

    cdef double[:] phsB                     ## spin phases of pulsar B, 0-1
    property phsB:
        def __get__(self):
            return np.asarray(self.phsB)
        def __set__(self, value):
            self.phsB = value

    cdef double[:] orbph                    ## orbital phases of pulsar A, 0-1
    property orbph:
        def __get__(self):
            return np.asarray(self.orbph)
        def __set__(self, value):
            self.orbph = value

    cdef double[:] flux                     ## flux of pulsar A
    property flux:
        def __get__(self):
            return np.asarray(self.flux)
        def __set__(self, value):
            self.flux = value

    cdef double[:] err                      ## flux errors of pulsar A
    property err:
        def __get__(self):
            return np.asarray(self.err)
        def __set__(self, value):
            self.err = value

    ##-------------------------------------------------------------------------
    def __init__(self, double[:] phsB, double[:] orbph, double[:] flux = None, double[:] err = None):
        self.n = phsB.shape[0]
        self.Bmax0 = 440.0
        self.rcool = 0.0
        self.phsB = phsB
        self.orbph = orbph
        if flux is None:
            self.flux = np.zeros(self.n, dtype=np.float64)
        else:
            self.flux = flux
        if err is None:
            self.err = np.ones(self.n, dtype=np.float64)
        else:
            self.err = err

    ##-------------------------------------------------------------------------
    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.nonecheck(False)
    cdef double[:] _Model(self, double theta=45*M_PI/180, double phi=60*M_PI/180, double chi=30*M_PI/180, double nu=0.8, double rmin=0.35, double upsilon=2., double z=-0.5, double ratio=1., double shift_phsB=0., int do_phase=1):
        cdef size_t i, j                ## loop variables
        cdef double minus_pownu_upsilon ## -upsilon * nu**-5/3, to simplify a calculation
        cdef double alpha               ## alpha is omega * t, the angle of the B-field vector with respect to the angular momentum vector
        cdef double x, y                ## x -> our line of sight, y -> lateral position (orbit) (units are in fraction of Rmax (i.e. Rabs+) ~ 1.5E9 cm)
        cdef double y2z2                ## y**2 + z**2
        cdef double deltax              ## increment in x that makes the radio beam travelling through the magnetosphere
        cdef int itot                   ## number of steps to ajust the position in x
        cdef double tau                 ## optical depth
        cdef double r                   ## radius, e.g. distance from pulsar B
        cdef double rmax                ## maximum radius reached by closed field lines (given by rmax=rmag)
        cdef double mu_x, mu_y, mu_z    ## components of the magnetic dipole vector
        cdef double cos_thetamu         ## cos_thetamu, i.e. thetamu is the angle between the radius vector r and the magnetic vector mu
        cdef double cos_kappa           ## cos_kappa, i.e. angle between the line of sight and the local direction of the B-field
        cdef double B                   ## absolute value of the B-field (units are in fraction of the magnetic moment muB = Bns*Rns**3)
        cdef double Bmax                ## maximum B-field for synchrotron resonant absorption (Bmax0 is defined in the constructor)

        ## Output placeholder
        cdef double[:] model = np.empty(self.n, dtype=np.float64)

        ## Initialization
        itot = 500
        Bmax = self.Bmax0 * nu
        minus_pownu_upsilon = - nu**(-5./3) * upsilon

        ## If required, calculate the phase shift due to the coordinate transformation
        if do_phase == 1:
            shift_phsB += Phase_on(theta, phi, chi)

        ## If required, calculate the phase shift due to the coordinate transformation, this time with a negative value
        if do_phase == -1:
            shift_phsB -= Phase_on(theta, phi, chi)

        for j in range(self.n):
            ## Here we calculate alpha
            alpha = (self.phsB[j] + shift_phsB) * 2*M_PI

            ## Here we calculate y
            ## x, y and z are expressed in Rmag units.
            ## Rmag corresponds to ratio*2*M_PI/360 times the orbital distance between pulsar A and B
            y = (self.orbph[j]-0.25)*360*ratio
            y2z2 = y**2 + z**2

            tau = 0.
            deltax = 0.

            if y2z2 < 1:
                ## Here we calculate the initial x value, we will step to different values of x
                x = sqrt(1-y2z2)
                ## We calculate the x step size
                deltax = 2*x/itot

                mu_z = cos(chi)*cos(theta)-sin(chi)*cos(alpha)*sin(theta)
                mu_x = (sin(chi)*cos(alpha)*cos(theta)+cos(chi)*sin(theta))*cos(phi) - sin(chi)*sin(alpha)*sin(phi)
                mu_y = (sin(chi)*cos(alpha)*cos(theta)+cos(chi)*sin(theta))*sin(phi) + sin(chi)*sin(alpha)*cos(phi)

                for i in range(itot):
                    x -= deltax

                    r = sqrt(x**2+y2z2)

                    if r > self.rcool:
                        cos_thetamu = (mu_z*z + mu_x*x + mu_y*y) / r

                        ## rmax = r**2 / (1 - cos_thetamu**2) / cos_thetamu; // for quadrupole?
                        ## rmax is the radius of the maximum extent of the field line: rmax = r/sin(theta_mu)^2 for dipole
                        ## If cos_thetamu**2 == 1, we're right above the pole and rmax = infinity. We set rmax = 1.1 so that we are "out" of the magnetosphere and there is no division by 0 later.
                        if cos_thetamu**2 == 1:
                            rmax = 1.1
                        else:
                            rmax = r / (1 - cos_thetamu**2)

                        if (rmax < 1) and (rmax > rmin):
                            ## B = sqrt(1 + 5*cos_thetamu**4 - 2*cos_thetamu**2) / r**4; // for quadrupole?
                            B = sqrt(1 + 3*cos_thetamu**2) / r**3

                            if B < Bmax:
                                ## cos_kappa = ((5*cos_thetamu**2-1)*x/r - 2*cos_thetamu*mu_x) / (r**4 * B); // for quadrupole?
                                cos_kappa = (3*cos_thetamu*x/r - mu_x) / (r**3 * B)
                                if cos_kappa > 1:
                                    cos_kappa = 1
                                elif cos_kappa < -1:
                                    cos_kappa = -1
                                tau += (B*sqrt(1-cos_kappa**2))**(2./3)

            model[j] = exp(minus_pownu_upsilon * tau * deltax)
        return model

    ##-------------------------------------------------------------------------
    def Model(self, theta=45*M_PI/180, phi=60*M_PI/180, chi=30*M_PI/180, nu=0.8, rmin=0.35, upsilon=2., z=-0.5, ratio=1., shift_phsB=0., do_phase=1):
        """
        Model

        Return the theoretical eclipse lightcurve given some input parameters.

        Parameters
        ----------
        theta : float
            Angle between the spin axis and the orbital angular momentum
            (radians).
            Default: 45 degrees
        phi : float
            Angle between the projection of the spin axis against the orbital
            plane and our line of sight (radians).
            Default: 60 degrees
        chi : float
            Angle between the spin axis and the magnetic axis (radians).
            Default: 30 degrees
        nu : float
            Radio frequency of the observation (GHz).
            Default: 0.8 GHz
        rmin : float
            Minimum radius for synchrotron absorption (mainly due to cooling).
            Default: 0.35
        upsilon : float
            Optical depth parameter (function of electron temperature and
            density).
            Default: 2.
        z : float
            Impact parameter, i.e. relative distance between pulsar A and B at
            their conjunction time. This can be used to infer the inclination.
            Default: -0.5
        ratio : float
            Mapping of the orbital phase to the unit coordinate system.
            Default: 1.
        shift_phsB : float
            Systematic phase shift to apply to the spin phase of pulsar B.
            Note: The phase is between 0-1, not 0-TWOPI.
            Default: 0.
        do_phase : int
            If equal to 1 (default value), will calculate the shift to apply to
            compensate for the coordinate system rotation when assigning the
            fiducial spin phase point.
            Note: A value of -1 can be used to subtract the phase shift rather
            than adding it. This is wrong, but permitted for debugging.
            Default: 1
        """
        return np.asarray(self._Model(theta=theta, phi=phi, chi=chi, nu=nu, rmin=rmin, upsilon=upsilon, z=z, ratio=ratio, shift_phsB=shift_phsB, do_phase=do_phase))

    ##-------------------------------------------------------------------------
    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.nonecheck(False)
    cdef double[:] _Residuals(self, double theta=45*M_PI/180, double phi=60*M_PI/180, double chi=30*M_PI/180, double nu=0.8, double rmin=0.35, double upsilon=2., double z=-0.5, double ratio=1., double shift_phsB=0., int do_phase=1):
        cdef size_t i                   ## loop variables

        ## Output placeholder
        cdef double[:] diff = np.empty(self.n, dtype=np.float64)

        model = self._Model(theta=theta, phi=phi, chi=chi, nu=nu, rmin=rmin, upsilon=upsilon, z=z, ratio=ratio, shift_phsB=shift_phsB, do_phase=do_phase)
        for i in range(self.n):
            diff[i] = (model[i] - self.flux[i]) / self.err[i]

        return diff

    ##-------------------------------------------------------------------------
    def Residuals(self, theta=45*M_PI/180, phi=60*M_PI/180, chi=30*M_PI/180, nu=0.8, rmin=0.35, upsilon=2., z=-0.5, ratio=1., shift_phsB=0., do_phase=1):
        """
        Residuals

        Calculate the residuals between the theoretical lightcurve and the
        data. The input parameters are the same as for the Model function.
        """
        return np.asarray(self._Residuals(theta=theta, phi=phi, chi=chi, nu=nu, rmin=rmin, upsilon=upsilon, z=z, ratio=ratio, shift_phsB=shift_phsB, do_phase=do_phase))

    ##-------------------------------------------------------------------------
    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.nonecheck(False)
    cpdef double Chi2(self, double theta=45*M_PI/180, double phi=60*M_PI/180, double chi=30*M_PI/180, double nu=0.8, double rmin=0.35, double upsilon=2., double z=-0.5, double ratio=1., double shift_phsB=0., int do_phase=1):
        """
        Chi2

        Calculate the Chi2 between the theoretical lightcurve and the
        data. The input parameters are the same as for the Model function.
        """
        cdef size_t i                   ## loop variables

        ## Output placeholder
        cdef double chi2
        chi2 = 0.

        model = self._Model(theta=theta, phi=phi, chi=chi, nu=nu, rmin=rmin, upsilon=upsilon, z=z, ratio=ratio, shift_phsB=shift_phsB, do_phase=do_phase)
        for i in range(self.n):
            chi2 += ((model[i] - self.flux[i]) / self.err[i])**2

        return chi2


cpdef double Phase_on(double theta, double phi, double chi):
    """
    Phase_on

    Return the phase of Omega*t which correspond to a pulse on.
    The phase is 0-1 NOT 0-TWOPI.

    The phase on corresponds to the magnetic moment pointing maximally towards
    Earth. Because Earth is in the x-direction, this is simply mu_x maximum.
        mu_x = (sin(chi)*cos(alpha)*cos(theta)+cos(chi)*sin(theta))*cos(phi) - sin(chi)*sin(alpha)*sin(phi)
    The local maximum is:
        max(mu_x) = d(mu_x)/d(alpha) = 0
                  = -sin(chi) * (sin(phi)*cos(alpha)+cos(theta)*sin(alpha)*cos(phi)) = 0
                  = (sin(phi)*cos(alpha)+cos(theta)*sin(alpha)*cos(phi))
                  -> alpha = arctan( -sin(alpha) / (cos(phi)*cos(theta)) )

        The maximum is for d^2(mu_x)/dalpha^2 < 0:
            sin(chi) * (sin(phi)*sin(alpha)-cos(theta)*cos(alpha)*cos(phi)) < 0
    """
    cdef double alpha
    alpha = atan2(-sin(phi), cos(phi)*cos(theta))
    if sin(chi) * (sin(phi)*sin(alpha) - cos(theta)*cos(alpha)*cos(phi)) >= 0:
        alpha += M_PI
    alpha /= 2*M_PI
    return alpha
