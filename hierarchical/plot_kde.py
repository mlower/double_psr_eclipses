import numpy as np
import bilby
import matplotlib.pyplot as plt
from scipy.stats import gaussian_kde
from utils import get_rc_params,get_median_and_bounds

def return_kde(samples, param_range):
    """
    Returns kernal density estimate of the input 1-D posterior
    samples.
    """
    kde = gaussian_kde(samples)
    kde_norm = kde.evaluate(param_range)/np.sum(
        kde.evaluate(param_range))

    return kde_norm

Omega_range = np.linspace(0, 10, 1024)

#joint = bilby.result.read_in_result("combined_test/label_result.json") 
#joint = bilby.result.read_in_result("output/label_result.json")
#Omega_joint = joint.posterior.Omega_SO.values

#"""
Lband = bilby.result.read_in_result("LBand_only/label_result.json")
Omega_Lband = Lband.posterior.Omega_SO.values

UHF = bilby.result.read_in_result("UHF_only/label_result.json")
Omega_UHF = UHF.posterior.Omega_SO.values
#"""

#joint_KDE = return_kde(Omega_joint, Omega_range)
Lband_KDE = return_kde(Omega_Lband, Omega_range)
UHF_KDE = return_kde(Omega_UHF, Omega_range)

#Breton = (1/np.sqrt(2 * np.pi * 0.66**2)) * np.exp(- ((Omega_range - 4.77)**2)/(2*0.66**2))

combined_KDE = Lband_KDE * UHF_KDE# * Breton

comb_samp = np.random.choice(Omega_range, p=combined_KDE/np.sum(combined_KDE), size=2048)
print(get_median_and_bounds(comb_samp))

#sigma_low = np.argwhere(Omega_range >= 4.95-0.53)[0][0]
#sigma_upp = np.argwhere(Omega_range >= 4.95+0.49)[0][0]

plt.rcParams.update(get_rc_params())
#plt.figure(figsize=(5,5))
plt.figure(figsize=(7,5))
#plt.plot(Omega_range, joint_KDE, lw=1, color="tab:blue", alpha=1.0, label="This work")
#plt.fill_between(Omega_range[sigma_low:sigma_upp], joint_KDE[sigma_low:sigma_upp],
#    color="tab:blue", alpha=0.5, zorder=0)
#plt.plot(Omega_range, joint_KDE, lw=1, color="k", alpha=0.5, label="Joint-fit")
plt.plot(Omega_range, Lband_KDE, lw=2, color="#01ac8b", label="Lband")
plt.plot(Omega_range, UHF_KDE, lw=2, ls="-", color="#188ac0", label="UHF")
plt.plot(Omega_range, combined_KDE/np.sum(combined_KDE), lw=2, ls="-", 
    color="tab:orange", alpha=0.5, label="Lband+UHF")
plt.fill_between(Omega_range, combined_KDE/np.sum(combined_KDE), color="tab:orange", alpha=0.5, zorder=0)
plt.axvline(4.76, ls="--", lw=2, color="k", label="Breton et al. (2008)")
plt.axvspan(4.12, 5.43, color="k", alpha=0.1)
#plt.axvspan(2.89, 6.90, color="#01ac8b", alpha=0.1)
#plt.plot(Omega_range, Breton/np.sum(Breton), lw=2, color="#01ac8b", label="Breton et al. (2008)")
plt.axvline(5.0734, lw=2, ls="-.", color="#eb3988", label="General relativity", alpha=0.6)
#plt.text(5.2, 0.011, "GR", color="#eb3988", fontsize=14)
plt.ylim(0, 0.013)
#plt.xlim(2.5,10)
plt.xlim(2.1, 7.5)
plt.xlabel(r"$\Omega_{\rm SO}^{\rm B}$ (deg/yr)")
plt.ylabel(r"$p(\Omega_{\rm SO}^{\rm B})$")
plt.tick_params(axis="both", bottom=True, top=True, left=True, right=True,
        direction="in", labelleft=False, labelbottom=True)

plt.legend(loc="upper left")
plt.tight_layout()
plt.savefig("Omega_comp_all.pdf")
plt.show()
