import types

import numpy as np
import pandas as pd
from scipy.stats import gamma

from bilby.core.likelihood import Likelihood
from bilby.hyper.model import Model

#try:
#    import cupy as xp
#    from cupyx.scipy.special import erf, gammaln

#    CUPY_LOADED = True
#except ImportError:
#    import numpy as xp
#    from scipy.special import erf, gammaln

#    CUPY_LOADED = False

import numpy as xp
from scipy.special import erf, gammaln

CUPY_LOADED = False

INF = xp.nan_to_num(xp.inf)


def to_numpy(array):
    """Cast any array to numpy"""
    if not CUPY_LOADED:
        return array
    else:
        return xp.asnumpy(array)


class HyperLikelihood(Likelihood):
    """
    Likelihood function for inferring population hyper-parameter distributions.

    See Eq. (34) of https://arxiv.org/abs/1809.02293 for a definition.

    Parameters
    ----------
    posteriors: dict
        Dictionary of posterior samples for each pulsar.
    hyper_prior: `bilby.hyper.model.Model`
        Population model, typically input as a function.
    hyper_parameters: dict
        Dictionary of population model hyper-paramters.
    ln_evidences: list
        List of log evidences for each pulsar.
    cupy: bool
       Use the cupy package if True and a compatible CUDA environment is 
       available. Note, requires setting up the hyper_prior properly.
    """
    
    def __init__(
        self, 
        time,
        posteriors, 
        hyper_prior, 
        hyper_parameters,
        ln_evidences, 
        cupy=True
    ):

        super().__init__()
        if cupy and not CUPY_LOADED:
            print("cupy not imported, using numpy...")

        self.time = time
        self.data = posteriors

        self.hyper_prior = hyper_prior
        self.parameters = hyper_parameters

        self.samples_per_posterior = np.log(np.shape(posteriors)[0])

        self.sampling_prior = 1.0
        self.ln_evidence = ln_evidences

    
    def log_likelihood_ratio(self):
        #print(self._compute_per_eclipse_ln_BF())
        ln_l = xp.sum(self._compute_per_eclipse_ln_BF())
        #print(ln_l)
        if xp.isnan(ln_l):
            return float(-INF)
        else:
            return float(xp.nan_to_num(ln_l))


    def log_likelihood(self):
        return self.log_likelihood_ratio()


    def _compute_per_eclipse_ln_BF(self):
        return (self.ln_evidence - self.samples_per_posterior) + xp.log(
            xp.sum(self.hyper_prior(self.time, self.data, **self.parameters) /\
            self.sampling_prior, axis=0)
        )
