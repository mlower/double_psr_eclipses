import numpy as np
import bilby
import glob
import random
from likelihood import HyperLikelihood
import matplotlib.pyplot as plt

def geodetic_precession(time, t_0, phi_0, Omega_SO):
    """
    Model describing the variation of the spin-axis longitude due to geodetic 
    precession.

    Parameters
    ----------
    time : ndarray, float
        Input time at which phi is being calcualted (yr).
    t_0 : float
        Reference time (yr)
    phi_0 : float 
        Value of phi at t_0 (deg)
    Omega_SO : float
        Geodetic precession rate (deg/yr).
    """
    return phi_0 - Omega_SO*((time - t_0)/365.25)


def ddf(time, data, **kwargs):
    """
    ...
    """
    offset = geodetic_precession(time, np.median(time), kwargs["phi_0"],
        kwargs["Omega_SO"])

    return (1/(abs(kwargs["sigma"])*np.sqrt(np.pi))) * np.exp(-((data-offset)/kwargs["sigma"])**2)


def gaussian(time, data, **kwargs):
    mu = geodetic_precession(time, np.median(time), kwargs["phi_0"],
        kwargs["Omega_SO"])
    return np.exp(-((data - mu)**2 / (2*kwargs["sigma"]**2))) / np.sqrt(2*np.pi*kwargs["sigma"]**2)


class ShannonLikelihood(bilby.likelihood.Likelihood):
    def __init__(self, time, samples, freqs, parameters):
        """
        Tailored Gaussian Likelihood

        A modified version of the Gaussian likelihood tailored for use on
        Double Pulsar eclipse data.

        Parameters
        ----------
        orbital_phase : ndarray
            Orbital phase corresponding to each point in the lightcurve.
        flux : ndarray, None
            Flux for each point in the lightcurve.
            If not provided, the array will be filled with zeros, which can be
            useful for testing and theoretical lightcurve calculations.
        flux_err : ndarray, None
            Uncertainties on the flux at each point in the lightcurve.
            If not provided, the array will be filled with zeros.
        date : str
            The date the observation was taken in ISO format.
        nu : float
            Radio frequency of the observation (GHz).
        parameters : dict_like
            Dictionary of parameter keys.
        """
        super().__init__()
        self.time = time
        self.samples = samples
        self.freqs = freqs
        self.parameters = parameters

    def log_likelihood(self):
        self.sigma = self.parameters["sigma"]
        self.nsamp = np.shape(self.samples)[0]
        self.neclipse = np.shape(self.time)

        self.t_0 = 53857.0#np.median(self.time)#57000.0#53857.0

        self.model = geodetic_precession(self.time, self.t_0, 
            self.parameters["phi_0"], self.parameters["Omega_SO"])

        self.residual = (self.samples - self.model)**2

        ln_like = np.sum(np.log(np.mean(np.exp(- (self.residual 
            / (2 * self.sigma**2))), axis=0)) - np.log(2 * np.pi 
            * self.sigma**2) / 2)

        return ln_like



# Extract posterior samples from individual eclipses
n_samp = 1000

#dates = np.genfromtxt("../pipeline/lists/temp_lband.list", dtype=str)
#dates = np.genfromtxt("../pipeline/lists/temp_uhf.list", dtype=str)
#dates = np.genfromtxt("../pipeline/lists/temp_all.list", dtype=str)
#dates = np.genfromtxt("../pipeline/lists/thesis_lband.list", dtype=str)
#dates = np.genfromtxt("../pipeline/lists/thesis_uhf.list", dtype=str)
dates = np.genfromtxt("../pipeline/lists/new_temp_all.list", dtype=str)

dir = "thesis"

freqs = np.array([])
mjds = np.array([])
lnZ = np.array([])
phi = np.zeros((n_samp, len(dates)))
med = np.array([])
std = np.array([])
for i in range(0, len(dates)):
    toa_file = np.genfromtxt("../pipeline/outdir/{0}/uncorr_{1}/ToA.tim".format(
        dir, dates[i]), dtype=float)
    freqs = np.append(freqs, toa_file[1])
    mjds = np.append(mjds, toa_file[2])

    result_file = "../pipeline/outdir/{0}/uncorr_{1}/label_result.json".format(
        dir, dates[i])
    old_result = bilby.result.read_in_result(result_file)

    #"""
    print(toa_file[1], toa_file[1] == 0.815734375)
    if toa_file[1] == 0.815734375:
        phi[:,i] = random.sample(old_result.posterior.phi.to_list(), n_samp)
        phi[:,i] = [j+4.69 for j in phi[:,i]]
        med = np.append(med, np.median(old_result.posterior.phi.values)+5.2)
    else:
        phi[:,i] = random.sample(old_result.posterior.phi.to_list(), n_samp)
        med = np.append(med, np.median(old_result.posterior.phi.values))#+4.69))
    #"""
    #phi[:,i] = random.sample(old_result.posterior.phi.to_list(), n_samp)
    lnZ = np.append(lnZ, old_result.log_evidence)
    #med = np.append(med, np.median(old_result.posterior.phi.values))#+4.69))
    std = np.append(std, np.std(old_result.posterior.phi.values))    

test = geodetic_precession(mjds, np.median(mjds), -6.0, 5.076)
plt.errorbar(mjds, med, yerr=std, fmt=".")
plt.plot(mjds, test)
plt.show()
#quit()

# Set likelihood
parameters = dict(phi_0=None, Omega_SO=None, sigma=None)
likelihood = HyperLikelihood(mjds, phi, ddf, parameters, lnZ)
#likelihood = ShannonLikelihood(mjds, phi, freqs, parameters)

# Set priors
priors = dict()
priors["phi_0"] = bilby.core.prior.Uniform(-90, 90, r"$\phi_{0}$ (deg)")
priors["Omega_SO"] = bilby.core.prior.Uniform(-10, 10, r"$\Omega_{\rm SO}$ (deg/yr)")
#priors["phi_0"] = bilby.core.prior.Gaussian(51.20, 0.8, r"$\phi_{0}$ (deg)")
#priors["Omega_SO"] = bilby.core.prior.Gaussian(4.77, 0.7, r"$\Omega_{\rm SO}$ (deg/yr)")
priors["sigma"] = bilby.core.prior.LogUniform(1e-3, 100, r"$\sigma$")

# Run Bilby
results = bilby.run_sampler(priors=priors, likelihood=likelihood,
        sampler="pymultinest", walks=100, nlive=1024, evidence_tolerance=0.1,
        importance_nested_sampling=True, resume=True,
        outdir="./test_hype")

# Plot posteriors
GR_pred = dict(phi_0=None, Omega_SO=5.0734, sigma=None)
results.plot_corner(truth=GR_pred, dpi=150)
plt.close()

print(np.median(results.posterior.Omega_SO.values))
