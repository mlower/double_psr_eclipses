import numpy as np
import os
import argparse


def get_input_arguments(parser):
    """
    Get the input arguments specified by the user.
    """
    parser.add_argument("-m", dest="mode", type=str,
        help="Set mode [single or joint]")

    return parser.parse_args()


parser = argparse.ArgumentParser()
args = get_input_arguments(parser)

if args.mode == "joint":
    eclipse_list = np.genfromtxt("./lists/dates_joint.list", delimiter=" ", dtype="str")

    for i in range(0, len(eclipse_list)):
        date1 = eclipse_list[i,0]
        date2 = eclipse_list[i,1]
        band = eclipse_list[i,2]
        label = eclipse_list[i,3]
        os.system("sbatch submit_slurm.sh {0} {1} {2} {3}".format(date1, date2, band, label))
        #os.system("sbatch submit_slurm.sh {0} {1} {2} uncorr_{3}".format(date1, date2, band, label))
        #print("sbatch submit_slurm.sh {0} {1} {2} {3}".format(date1, date2, band, label))


elif args.mode == "single":
    eclipse_list = np.genfromtxt("./lists/dates_n_freqs.list", delimiter=" ", dtype="str")

    for i in range(0, len(eclipse_list)):
        date = eclipse_list[i,0]
        band = eclipse_list[i,1]
        os.system("sbatch submit_slurm.sh {0} {1}".format(date, band))
        #print("sbatch submit_slurm.sh {0} {1}".format(date, band))

else:
    raise ValueError("No option (joint or single) was selected")
