import numpy as np
import argparse
import bilby
import time as runtime
import matplotlib.pyplot as plt

from astropy.time import Time
from likelihood import TailoredGaussianLikelihood
from template import generate_eclipse

# Global params
SEC_PER_DAY = 24*60*60
DEG_TO_RAD = np.pi/180

def get_input_arguments(parser):
    """
    Get the input arguments specified by the user.
    """
    parser.add_argument("-e", dest="eclipse_file", type=str,
        help="Input .eclipse file.")
    parser.add_argument("-b", dest="band", type=str,
        help="Frequency band [Lband, UHF]")
    parser.add_argument("-l", dest="label", type=str, help="Output label")
    
    return parser.parse_args()


def get_rc_params():
    """
    Get the rcParams that will be used in all the plots.
    Run: plt.rcParams.update(get_rc_params())
    """

    rc_params = {
        "text.usetex": True,
        "font.family": "serif",

        "figure.dpi": 125,
        "legend.fontsize": 10,
        "legend.frameon": True,
        "legend.markerscale": 1.0,

        "axes.labelsize": 12,
        #"axes.edgecolor": 'white',
        #"axes.labelcolor": 'white',

        #"xtick.direction": 'in',
        "xtick.labelsize": 12,
        "xtick.minor.visible": True,
        #"xtick.top": True,
        "xtick.major.width": 1,
        #"xtick.color": 'white',

        #"ytick.direction": 'in',
        "ytick.labelsize": 12,
        "ytick.minor.visible": True,
        #"ytick.right": True,
        "ytick.major.width": 1,
        #"ytick.color": 'white',

        "savefig.transparent": False,
    }

    return rc_params


def compute_B_period(MJD):
    return 2.77346077007 + (8.92e-16 * (MJD - 53156.0))


class eclipse_data():
    """
    Unpack data from an input eclipse textfile.
    """
    def __init__(self, filename):

        self.datafile = np.genfromtxt(filename, skip_header=4,
            delimiter=" ")
        self.orbital_phase = self.datafile[:,1]/360.
        self.on_eclipse = np.transpose(np.argwhere(
            (self.orbital_phase >= 0.247) & (self.orbital_phase <= 0.2517)))[0]

    def get_date_mjd(self):
        return self.datafile[self.on_eclipse,0][0]

    def get_on_eclipse(self):
        return self.on_eclipse

    def get_time(self):
        self.date_mjd = self.get_date_mjd()
        return (self.datafile[:,0] - self.date_mjd) * SEC_PER_DAY

    def get_orbital_phase(self):
        return self.orbital_phase

    def get_flux(self):
        self.flux = ( self.datafile[:,2]
            /np.median(self.datafile[:self.on_eclipse[0],2]) )
        return self.flux

    def get_flux_err(self):
        self.flux_err = ( self.datafile[:,3]
            /np.median(self.datafile[:self.on_eclipse[0],2]) )
        return self.flux_err


# If run directly
if __name__ == "__main__":
    Start = runtime.time()

    #outdir = "/fred/oz005/users/mlower/double_psr/double_psr_eclipses/pipeline/outdir"
    outdir = "./outdir"

    parser = argparse.ArgumentParser()
    args = get_input_arguments(parser)

    label_split_1 = args.label.split("-")
    label_split_2 = label_split_1[3].split(":")
    
    obs_label = "eclipse_{0}{1}{2}_{3}{4}{5}".format(label_split_1[0], 
        label_split_1[1], label_split_1[2], label_split_2[0], label_split_2[1], 
        label_split_2[2])

    if args.band == "Lband":
        freq = 1.28446
    elif args.band == "UHF":
        freq = 0.81573
    elif args.band == "GBT820":
        freq = 0.82000
    else:
        raise ValueError("Frequency band (currently) unsupported.")

    eclipse = eclipse_data(args.eclipse_file)

    # Get spin period of B at eclipse date
    P0_B = compute_B_period(eclipse.get_date_mjd())
    time = eclipse.get_time() - eclipse.get_time()[eclipse.get_on_eclipse()[0]]

    params = dict(P0_B=P0_B, theta=139.44, chi=62.57, nu=freq, r_min=0.5, 
       upsilon=2.63, z=-0.57, ratio=0.96, delta_phi_B=0.46, phi=2.38,
       do_phase=1)

    params = dict(P0_B=P0_B, theta=139.44, chi=62.57, nu=freq, r_min=0.5,
       upsilon=2.63, z=-0.57, ratio=0.96, delta_phi_B=0.75, phi=2.38,
       do_phase=1)

    med = generate_eclipse(time, eclipse.get_orbital_phase(), eclipse.get_flux(),
        freq, **params)

    # upp
    params = dict(P0_B=P0_B, theta=139.44+0.83, chi=62.57+0.98, nu=freq, r_min=0.5,
       upsilon=2.63+0.13, z=-0.57+0.01, ratio=0.96+0.02, delta_phi_B=0.46+0.01, phi=2.38+1.55,
       do_phase=1)

    upp = generate_eclipse(time, eclipse.get_orbital_phase(), eclipse.get_flux(),
        freq, **params)

    # low
    params = dict(P0_B=P0_B, theta=139.44-0.88, chi=62.57-0.95, nu=freq, r_min=0.5,
       upsilon=2.63-0.14, z=-0.57-0.01, ratio=0.96-0.02, delta_phi_B=0.46-0.01, phi=2.38-1.72,
       do_phase=1)

    low = generate_eclipse(time, eclipse.get_orbital_phase(), eclipse.get_flux(),
        freq, **params)


    plt.rcParams.update(get_rc_params())
    plt.figure(figsize=(4,3))
#    plt.errorbar(eclipse.get_orbital_phase()*360, eclipse.get_flux(),
#        yerr=eclipse.get_flux_err(), fmt=".", color="k", ecolor="tab:grey")
#    plt.plot(eclipse.get_orbital_phase()*360, eclipse.get_flux(), 
#        color="k", lw=1)
    plt.plot(eclipse.get_orbital_phase()*360, med, color="#495f8a", lw=1)
#    plt.fill_between(eclipse.get_orbital_phase()*360, low, upp, color="#eb348f", alpha=0.5)
    #plt.axvline(90.0, ls="--", color="tab:red")
    #plt.xlim(88.5, 91.5)
    plt.xlim(88.8, 91.2)
    plt.ylim(-0.3, 1.4)
    plt.xlabel("Orbital phase (deg)")
    plt.ylabel("Intensity (arb. units)")
    plt.tight_layout()
    #plt.savefig("./figures/{0}.pdf".format(obs_label))
    plt.show()

    quit()

    # re-make the corner-plot

    result = bilby.result.read_in_result(
        "./outdir/{0}/label_result.json".format(obs_label))

    parameters = ["delta_phi_B", "theta", "chi", "phi", "r_min", "upsilon", "z",
        "ratio"]

    result.plot_corner(parameters=parameters, priors=True, plot_contours=True, dpi=150)

    quit()

    # Save effective ToA
    #phi_B = results.posterior.delta_phi_B.values
    #P_B = results.posterior.P0_B.values

    #ToA_offset = (phi_B * P_B) / SEC_PER_DAY

    #with open("./outdir/{0}/ToA.tim".format(obs_label), "a") as toa_file:
    #    toa_file.write("test 1283.58203125 {0} {1} bat\n".format(
    #        eclipse.get_date_mjd() + np.mean(ToA_offset),
    #        np.std(ToA_offset)))
    #    toa_file.close()
