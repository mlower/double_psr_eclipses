# Utility functions
import numpy as np

def get_rc_params():
    """
    Get the rcParams that will be used in all the plots.
    Run: plt.rcParams.update(get_rc_params())
    """

    rc_params = {
        "text.usetex": True,
        "font.family": "serif",

        "figure.dpi": 125,
        "legend.fontsize": 12,
        "legend.frameon": False,
        "legend.markerscale": 1.0,

        "axes.labelsize": 14,

        "xtick.direction": 'in',
        "xtick.labelsize": 14,
        "xtick.minor.visible": True,
        "xtick.top": True,
        "xtick.major.width": 1,

        "ytick.direction": 'in',
        "ytick.labelsize": 14,
        "ytick.minor.visible": True,
        "ytick.right": True,
        "ytick.major.width": 1,

        "savefig.transparent": False,
    }

    return rc_params


def get_median_and_bounds(posterior, nbins=80):
    pdf, vals = np.histogram(posterior, nbins)
    pdf = list(np.float_(pdf))

    pdf_normalised = pdf/np.sum(pdf)
    cdf = np.cumsum(pdf_normalised)

    median = vals[np.argmin(np.abs(cdf - 0.5))]
    low_bound = vals[np.argmin(np.abs(cdf - 0.16))]
    upp_bound = vals[np.argmin(np.abs(cdf - 0.84))]

    return median, low_bound, upp_bound
