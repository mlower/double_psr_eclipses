import numpy as np
import bilby
import time
import sys
import matplotlib.pyplot as plt

from astropy.time import Time
from likelihood import TailoredGaussianLikelihood
from template import generate_eclipse

Start = time.time()

# Global params
SEC_PER_DAY = 24*60*60
DEG_TO_RAD = np.pi/180

eclipse = str(sys.argv[1])

datafile = np.genfromtxt("../../eclipses/{0}/{0}.eclipse".format(eclipse),
    skip_header=4, delimiter=" ")

date_mjd = datafile[0,0]
orbital_phase = datafile[:,1]/360.

#on_eclipse = np.transpose(np.argwhere(
#    (orbital_phase >= 0.247) & (orbital_phase <= 0.2515)))[0]

#orbital_phase = orbital_phase[on_eclipse]
#flux = datafile[on_eclipse,2]/np.median(datafile[:on_eclipse[0],2])
#flux_err = datafile[on_eclipse,3]/np.median(datafile[:on_eclipse[0],2])

#orbital_phase = orbital_phase[on_eclipse[0]:]
#flux = datafile[on_eclipse[0]:,2]/np.median(datafile[:on_eclipse[0],2])
#flux_err = datafile[on_eclipse[0]:,3]/np.median(datafile[:on_eclipse[0],2])

orbital_phase = datafile[:,1]/360.
on_eclipse = np.transpose(np.argwhere(
    (orbital_phase >= 0.247) & (orbital_phase <= 0.2517)))[0]

time = (datafile[on_eclipse,0] - datafile[on_eclipse,0][0]) * SEC_PER_DAY
orbital_phase = orbital_phase[on_eclipse]
flux = datafile[on_eclipse,2]/np.median(datafile[:on_eclipse[0],2])
flux_err = datafile[on_eclipse,3]/np.median(datafile[:on_eclipse[0],2])

# 2021-04-21-18:47:37
kwargs = dict(delta_phi_B=0.75, theta=130.97, chi=70.73, phi=-12.95, r_min=0.5,
    upsilon=2.0, z=-0.5, ratio=1.0, P0_B=2.77346077007)


template = generate_eclipse(time, orbital_phase, flux, date=date_mjd, nu=0.81573, **kwargs)
"""
template1 = generate_eclipse(time, orbital_phase, flux, date=date_mjd, nu=0.5, **kwargs)
template2 = generate_eclipse(time, orbital_phase, flux, date=date_mjd, nu=0.6, **kwargs)
template3 = generate_eclipse(time, orbital_phase, flux, date=date_mjd, nu=0.7, **kwargs)
template4 = generate_eclipse(time, orbital_phase, flux, date=date_mjd, nu=0.8, **kwargs)
template5 = generate_eclipse(time, orbital_phase, flux, date=date_mjd, nu=0.9, **kwargs)
template6 = generate_eclipse(time, orbital_phase, flux, date=date_mjd, nu=1.0, **kwargs)
"""

#flux = template + np.random.normal(0.0, 0.15, len(template))

# Plot the simulated data
#plt.errorbar(orbital_phase, flux, yerr=flux_err, fmt=".", color="k", ecolor="tab:grey")
#plt.plot(orbital_phase, flux, lw=1, color="k")
#plt.plot(orbital_phase, template, lw=2, color="tab:orange")

mean_temp = np.zeros((500,len(orbital_phase)))
for i in range(500, 1000):
    temp = generate_eclipse(time, orbital_phase, flux, date=date_mjd, nu=float(i)/1000, **kwargs)
    #plt.plot(orbital_phase, temp+0.01, lw=2)

    mean_temp[i-500,:] = temp
    #mean_temp = mean_temp + temp


plt.imshow(mean_temp, aspect="auto")
#plt.plot(orbital_phase, mean_temp/500.0, lw=1, color="k")
#plt.errorbar(orbital_phase, flux-template, yerr=flux_err, fmt=".", color="k", ecolor="tab:grey")
#plt.plot(orbital_phase, flux-template, lw=1, color="k", alpha=0.5)
#plt.xlim(0.247, 0.2525)
#plt.ylim(-1, 2)
plt.show()

quit()

plt.plot(orbital_phase, template, lw=1)
plt.plot(orbital_phase, np.mean(mean_temp, axis=0), color="k", lw=1)
plt.xlabel("Orbital phase")
#plt.ylabel("Normalised flux (arb. units)")
#plt.ylabel("Residuals (arb. units)")
plt.tight_layout()
#plt.savefig("./plots/{0}.png".format(eclipse), dpi=150)
plt.show()
plt.close()

quit()

# Set-up priors
parameters = dict(delta_phi_B=None, P0_B=None, theta=None, chi=None, 
    phi=None, r_min=None, upsilon=None, z=None, ratio=None, sigma=None)

#priors = dict()
#priors["delta_phi_B"] = bilby.core.prior.Uniform(0, 1, r"$\Delta\phi_{\rm B}$")
#priors["P0_B"] = bilby.core.prior.Uniform(2.65, 2.95, r"$P_{\rm B}$")
#priors["P0_B"] = bilby.core.prior.Gaussian(2.77346122494, 1e-11, r"$P_{\rm B}$")
#priors["P0_B"] = bilby.core.prior.DeltaFunction(2.77346122494, r"$P_{\rm B}$")
#priors["theta"] = bilby.core.prior.Gaussian(130, 30, r"$\theta$")
#priors["chi"] = bilby.core.prior.Gaussian(70.9, 30, r"$\chi$")
#priors["phi"] = bilby.core.prior.Uniform(-90, 90, r"$\phi_{\rm B}$")
#priors["Omega_B"] = bilby.core.prior.Uniform(0, 10, r"$\Omega_{\rm B}$")
#priors["r_min"] = bilby.core.prior.Uniform(0, 1, r"$r_{\rm min}$")
#priors["upsilon"] = bilby.core.prior.Uniform(0, 10, r"$\upsilon$")
#priors["z"] = bilby.core.prior.Uniform(-5, 5, r"$z_{0}/R_{\rm mag}$")
#priors["sigma"] = bilby.core.prior.Uniform(0, 2, r"$\sigma_{\rm noise}$")

priors = dict()
priors["delta_phi_B"] = bilby.core.prior.Uniform(0, 1, r"$\Delta\phi_{\rm B}$")
#priors["P0_B"] = bilby.core.prior.Uniform(2.65, 2.8, r"$P_{\rm B}$")
priors["P0_B"] = bilby.core.prior.DeltaFunction(2.77346122494, r"$P_{\rm B}$")
priors["theta"] = bilby.core.prior.Uniform(90, 180, r"$\theta$")
priors["chi"] = bilby.core.prior.Uniform(0, 90, r"$\chi$")
priors["phi"] = bilby.core.prior.Uniform(-90, 90, r"$\phi_{\rm B}$")
#priors["Omega_B"] = bilby.core.prior.Uniform(0, 10, r"$\Omega_{\rm B}$")
priors["r_min"] = bilby.core.prior.Uniform(0, 1, r"$r_{\rm min}$")
priors["upsilon"] = bilby.core.prior.Uniform(0, 4, r"$\upsilon$")
priors["z"] = bilby.core.prior.Uniform(-2, 2, r"$z_{0}/R_{\rm mag}$")
priors["ratio"] = bilby.core.prior.Uniform(0, 2, r"$\xi$")
priors["sigma"] = bilby.core.prior.Uniform(0, 2, r"$\sigma_{\rm noise}$")

# Define the likelihood for model 1
likelihood = TailoredGaussianLikelihood(orbital_phase=orbital_phase, flux=flux,
    flux_err=flux_err, date="2020-07-28T04:45:33.8544", nu=1.28446,
    parameters=parameters)

# Run sampling with dynesty
results = bilby.run_sampler(priors=priors, likelihood=likelihood, sampler="dynesty", nlive=2048,
                            nwalks=32, outdir="../real_test/full_test", label="uniform_priors_fixed_spin")

# Gaussian_priors/extend_P0_

Finish = time.time()

injected_params = dict(delta_phi_B=None, P0_B=None, theta=130.02, chi=70.94,
    phi=None, r_min=None, upsilon=2.0, z=-0.543, ratio=1.28, sigma=None)

results.plot_corner(truth=injected_params, priors=True, plot_contours=False)
plt.close()

print("Parameter estimation took {0} seconds to complete".format(Finish - Start))

