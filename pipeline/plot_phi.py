import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import bilby
import glob
from utils import get_rc_params


def linear_fit(x, Omega_SO, phi_0):
    return phi_0 - (Omega_SO*(x))

dates_lband = np.genfromtxt("./lists/lband.list", dtype=str)
dates_uhf = np.genfromtxt("./lists/uhf.list", dtype=str)

#dir = "thesis"
#dir = "no_offset"
#dir = "tempo1_timing"
#dir = "nope_tempo1"
#dir = "fix_nu_nope_tempo1"
dir = "fix_nu_yes_tempo1"

mjds_lband = []
phi_lband = []
alpha_lband = []
theta_lband = []
upsilon_lband = []
z_0_lband = []
ratio_lband = []
dorb_1_lband = []
dorb_2_lband = []
for i in range(0, len(dates_lband)):
    if dates_lband[i] == "joint_190326" or dates_lband[i] == "joint_210820":
        pass
    else:    
        toa_file = np.genfromtxt("./outdir/{0}/{1}/ToA.tim".format(
            dir, dates_lband[i]), dtype=float)
        mjds_lband.append(toa_file[2])

        result_file = "./outdir/{0}/{1}/label_result.json".format(
            dir, dates_lband[i])
        result = bilby.result.read_in_result(result_file)
        phi_lband.append(result.posterior.phi.values)# - 4.8)
        alpha_lband.append(result.posterior.chi.values)# - 3.6)
        theta_lband.append(result.posterior.theta.values)# - 12.6)
        upsilon_lband.append(result.posterior.upsilon.values)
        z_0_lband.append(result.posterior.z.values)
        ratio_lband.append(result.posterior.ratio.values)
        dorb_1_lband.append(result.posterior.dorb_1.values * 360.0)
        dorb_2_lband.append(result.posterior.dorb_2.values * 360.0)

mjds_uhf = []
phi_uhf = []
alpha_uhf = []
theta_uhf = []
upsilon_uhf = []
z_0_uhf = []
ratio_uhf = []
dorb_1_uhf = []
dorb_2_uhf = []
for i in range(0, len(dates_uhf)):
    toa_file = np.genfromtxt("./outdir/{0}/{1}/ToA.tim".format(
        dir, dates_uhf[i]), dtype=float)
    mjds_uhf.append(toa_file[2])

    result_file = "./outdir/{0}/{1}/label_result.json".format(
        dir, dates_uhf[i])
    result = bilby.result.read_in_result(result_file)
    phi_uhf.append(result.posterior.phi.values)# + 4.69)
    alpha_uhf.append(result.posterior.chi.values)# - 3.6)
    theta_uhf.append(result.posterior.theta.values)# - 12.6)
    upsilon_uhf.append(result.posterior.upsilon.values)
    z_0_uhf.append(result.posterior.z.values)
    ratio_uhf.append(result.posterior.ratio.values)
    dorb_1_uhf.append(result.posterior.dorb_1.values * 360.0)
    dorb_2_uhf.append(result.posterior.dorb_2.values * 360.0)

mjds = np.sort(np.append(mjds_lband, mjds_uhf))
mjd_0 = np.median(mjds)
mjd_pred = np.linspace(mjds[0]-100, mjds[-1]+100, 1024) - mjd_0
yrs = (mjds - mjd_0)/365.25

"""
med_alpha_lband = []
med_alpha_uhf = []
med_theta_lband = []
med_theta_uhf = []
for i in range(0, len(alpha_lband)):
    med_alpha_lband.append(np.median(alpha_lband[i]))
    med_alpha_uhf.append(np.median(alpha_uhf[i]))

    med_theta_lband.append(np.median(theta_lband[i]))
    med_theta_uhf.append(np.median(theta_uhf[i]))


phi_med_lba = []
phi_std_lba = []
phi_med_uhf = []
phi_std_uhf = []
for i in range(0, len(alpha_lband)):
    phi_med_lba.append(np.median(phi_lband[i]))
    phi_std_lba.append(np.std(phi_lband[i])) 

for i in range(0, len(alpha_uhf)):
    phi_med_uhf.append(np.median(phi_uhf[i]))
    phi_std_uhf.append(np.std(phi_uhf[i]))

alpha_diff = np.median(med_alpha_lband) - np.median(med_alpha_uhf)
theta_diff = np.median(med_theta_lband) - np.median(med_theta_uhf)
"""

#fit_tw_med = linear_fit(mjd_pred/365.25, 8.16, 5.98)
fit_tw_med = linear_fit(mjd_pred/365.25, 5.42, 7.75)

#fit_tw_med = linear_fit(mjd_pred/365.25, 5.02, -1.85)
#fit_tw_upp = linear_fit(mjd_pred/365.25, 5.02+0.52, -1.85+0.42)
#fit_tw_low = linear_fit(mjd_pred/365.25, 5.02-0.50, -1.85-0.27)

#fit_tw_med = linear_fit(mjd_pred/365.25, 4.95, -4.40)
#fit_tw_upp = linear_fit(mjd_pred/365.25, 4.95+0.49, -4.40+0.31)
#fit_tw_low = linear_fit(mjd_pred/365.25, 4.95-0.53, -4.40-0.31)

fit_Breton_med = linear_fit(mjd_pred/365.25, 4.76, 7.51)
#fit_Breton_upp = linear_fit(mjd_pred/365.25, 5.43, -4.68+0.41)
#fit_Breton_low = linear_fit(mjd_pred/365.25, 4.12, -4.68-0.42)

#mjd_B08 = np.linspace(mjds[0]-100, mjds[-1]+100, 1024) - 53857.0
#fit_Breton_old = linear_fit(mjd_B08/365.25, 4.76, 51.21)

#"""
plt.rcParams.update(get_rc_params())

plt.plot(-np.inf, np.inf, "o", color='#01ac8b', markersize=4, label="L-band")
plt.plot(-np.inf, np.inf, "o", color='#188ac0', markersize=4, label="UHF")

violin_parts = plt.violinplot(dorb_1_lband, mjds_lband, widths=20, showextrema=False)
for pc in violin_parts['bodies']:
    pc.set_facecolor('#01ac8b')
    pc.set_edgecolor('#01ac8b')
    pc.set_linewidth(1)
    pc.set_alpha(0.7)

violin_parts = plt.violinplot(dorb_2_lband, mjds_lband, widths=20, showextrema=False)
for pc in violin_parts['bodies']:
    pc.set_facecolor('#01ac8b')
    pc.set_edgecolor('#01ac8b')
    pc.set_linewidth(1)
    pc.set_alpha(0.7)

violin_parts = plt.violinplot(dorb_1_uhf, mjds_uhf, widths=20, showextrema=False)
for pc in violin_parts['bodies']:
    pc.set_facecolor('#188ac0')
    pc.set_edgecolor('#188ac0')
    pc.set_linewidth(1)
    pc.set_alpha(0.7)

violin_parts = plt.violinplot(dorb_2_uhf, mjds_uhf, widths=20, showextrema=False)
for pc in violin_parts['bodies']:
    pc.set_facecolor('#188ac0')
    pc.set_edgecolor('#188ac0')
    pc.set_linewidth(1)
    pc.set_alpha(0.7)

plt.xlim(mjds[0]-100, mjds[-1]+100)
plt.xlabel(r"Time (MJD)")
plt.ylabel(r"$\delta \phi_{\rm orb-phase}$ (deg)")
plt.legend(loc="best")
plt.tight_layout()
plt.show()
plt.close
#quit()
#"""

plt.rcParams.update(get_rc_params())

plt.plot(-np.inf, np.inf, "o", color='#01ac8b', markersize=4, label="L-band")
plt.plot(-np.inf, np.inf, "o", color='#188ac0', markersize=4, label="UHF")

violin_parts = plt.violinplot(phi_lband, mjds_lband, widths=20, showextrema=False)
for pc in violin_parts['bodies']:
    pc.set_facecolor('#01ac8b')
    pc.set_edgecolor('#01ac8b')
    pc.set_linewidth(1)
    pc.set_alpha(0.7)

violin_parts = plt.violinplot(phi_uhf, mjds_uhf, widths=20, showextrema=False)
for pc in violin_parts['bodies']:
    pc.set_facecolor('#188ac0')
    pc.set_edgecolor('#188ac0')
    pc.set_linewidth(1)
    pc.set_alpha(0.7)

plt.plot(mjd_pred+mjd_0, fit_tw_med, ls="-.", color="tab:red", label="This work")

plt.plot(mjd_pred+mjd_0, fit_Breton_med, ls="--", color="tab:orange",
    label="Breton et al. (2008)")

#plt.plot(mjd_B08+53857.0, fit_Breton_old, ls="--", color="tab:orange",
#    label="Breton et al. (2008)")

plt.xlim(mjds[0]-100, mjds[-1]+100)
plt.xlabel(r"Time (MJD)")
plt.ylabel(r"$\varphi_{\rm B}$ (deg)")
plt.legend(loc="best")
plt.tick_params(axis="both", bottom=True, top=True, left=True, right=True,
        direction="in", labelbottom=True)

plt.show()
plt.close()
#quit()

#"""
#-----------------------------------------------------------------------------#

plt.rcParams.update(get_rc_params())
fig = plt.figure(figsize=(8,8))
grid = plt.GridSpec(9, 6, hspace=0.08)

alp_ax = fig.add_subplot(grid[:3,:])
the_ax = fig.add_subplot(grid[3:6,:], sharex=alp_ax)
phi_ax = fig.add_subplot(grid[6:,:], sharex=alp_ax)

phi_ax.plot(-np.inf, np.inf, "o", color='#01ac8b', markersize=4, label="L-band")
phi_ax.plot(-np.inf, np.inf, "o", color='#188ac0', markersize=4, label="UHF")

violin_parts = alp_ax.violinplot(alpha_lband, mjds_lband, widths=20, showextrema=False)
for pc in violin_parts['bodies']:
    pc.set_facecolor('#01ac8b')
    pc.set_edgecolor('#01ac8b')
    pc.set_linewidth(1)
    pc.set_alpha(0.7)

violin_parts = the_ax.violinplot(theta_lband, mjds_lband, widths=20, showextrema=False)
for pc in violin_parts['bodies']:
    pc.set_facecolor('#01ac8b')
    pc.set_edgecolor('#01ac8b')
    pc.set_linewidth(1)
    pc.set_alpha(0.7)

violin_parts = phi_ax.violinplot(phi_lband, mjds_lband, widths=20, showextrema=False)
for pc in violin_parts['bodies']:
    pc.set_facecolor('#01ac8b')
    pc.set_edgecolor('#01ac8b')
    pc.set_linewidth(1)
    pc.set_alpha(0.7)

violin_parts = alp_ax.violinplot(alpha_uhf, mjds_uhf, widths=20, showextrema=False)
for pc in violin_parts['bodies']:
    pc.set_facecolor('#188ac0')
    pc.set_edgecolor('#188ac0')
    pc.set_linewidth(1)
    pc.set_alpha(0.7)

violin_parts = the_ax.violinplot(theta_uhf, mjds_uhf, widths=20, showextrema=False)
for pc in violin_parts['bodies']:
    pc.set_facecolor('#188ac0')
    pc.set_edgecolor('#188ac0')
    pc.set_linewidth(1)
    pc.set_alpha(0.7)

violin_parts = phi_ax.violinplot(phi_uhf, mjds_uhf, widths=20, showextrema=False)
for pc in violin_parts['bodies']:
    pc.set_facecolor('#188ac0')
    pc.set_edgecolor('#188ac0')
    pc.set_linewidth(1)
    pc.set_alpha(0.7)


#ax1.plot(mjd_pred+mjd_0, fit_Breton_med, color="k", ls="--", lw=1, label=r"(Breton et al. 2008)")
#ax1.fill_between(mjd_pred+mjd_0, fit_Breton_upp, fit_Breton_low, color="k", alpha=0.4)

#ax1.plot(mjd_pred+mjd_0, fit_tw_med, color="#eb348f", ls="-", lw=1, label=r"This work")
#ax1.fill_between(mjd_pred+mjd_0, fit_tw_upp, fit_tw_low, color="#eb348f", alpha=0.4)

alp_ax.set_xlim(mjds[0]-100, mjds[-1]+100)
alp_ax.set_ylabel(r"$\alpha$ (deg)")
alp_ax.tick_params(axis="both", bottom=True, top=True, left=True, right=True,
        direction="in", labelbottom=False)

the_ax.set_xlim(mjds[0]-100, mjds[-1]+100)
the_ax.set_ylabel(r"$\theta$ (deg)")
the_ax.tick_params(axis="both", bottom=True, top=True, left=True, right=True,
        direction="in", labelbottom=False)

phi_ax.set_xlim(mjds[0]-100, mjds[-1]+100)
phi_ax.set_xlabel(r"Time (MJD)")
phi_ax.set_ylabel(r"$\varphi_{\rm B}$ (deg)")
phi_ax.legend(loc="best")
phi_ax.tick_params(axis="both", bottom=True, top=True, left=True, right=True,
        direction="in", labelbottom=True)

#plt.savefig("../b_psr_geometry.pdf")
plt.show()
#"""

#-----------------------------------------------------------------------------#

plt.rcParams.update(get_rc_params())
fig = plt.figure(figsize=(8,8))
grid = plt.GridSpec(9, 6, hspace=0.08)

alp_ax = fig.add_subplot(grid[:3,:])
the_ax = fig.add_subplot(grid[3:6,:], sharex=alp_ax)
phi_ax = fig.add_subplot(grid[6:,:], sharex=alp_ax)

phi_ax.plot(-np.inf, np.inf, "o", color='#01ac8b', markersize=4, label="L-band")
phi_ax.plot(-np.inf, np.inf, "o", color='#188ac0', markersize=4, label="UHF")

violin_parts = alp_ax.violinplot(upsilon_lband, mjds_lband, widths=20, showextrema=False)
for pc in violin_parts['bodies']:
    pc.set_facecolor('#01ac8b')
    pc.set_edgecolor('#01ac8b')
    pc.set_linewidth(1)
    pc.set_alpha(0.7)

violin_parts = the_ax.violinplot(z_0_lband, mjds_lband, widths=20, showextrema=False)
for pc in violin_parts['bodies']:
    pc.set_facecolor('#01ac8b')
    pc.set_edgecolor('#01ac8b')
    pc.set_linewidth(1)
    pc.set_alpha(0.7)

violin_parts = phi_ax.violinplot(ratio_lband, mjds_lband, widths=20, showextrema=False)
for pc in violin_parts['bodies']:
    pc.set_facecolor('#01ac8b')
    pc.set_edgecolor('#01ac8b')
    pc.set_linewidth(1)
    pc.set_alpha(0.7)

violin_parts = alp_ax.violinplot(upsilon_uhf, mjds_uhf, widths=20, showextrema=False)
for pc in violin_parts['bodies']:
    pc.set_facecolor('#188ac0')
    pc.set_edgecolor('#188ac0')
    pc.set_linewidth(1)
    pc.set_alpha(0.7)

violin_parts = the_ax.violinplot(z_0_uhf, mjds_uhf, widths=20, showextrema=False)
for pc in violin_parts['bodies']:
    pc.set_facecolor('#188ac0')
    pc.set_edgecolor('#188ac0')
    pc.set_linewidth(1)
    pc.set_alpha(0.7)

violin_parts = phi_ax.violinplot(ratio_uhf, mjds_uhf, widths=20, showextrema=False)
for pc in violin_parts['bodies']:
    pc.set_facecolor('#188ac0')
    pc.set_edgecolor('#188ac0')
    pc.set_linewidth(1)
    pc.set_alpha(0.7)

alp_ax.set_xlim(mjds[0]-100, mjds[-1]+100)
alp_ax.set_ylabel(r"$\upsilon$")
alp_ax.tick_params(axis="both", bottom=True, top=True, left=True, right=True,
        direction="in", labelbottom=False)

the_ax.set_xlim(mjds[0]-100, mjds[-1]+100)
the_ax.set_ylabel(r"$z_{0}$")
the_ax.tick_params(axis="both", bottom=True, top=True, left=True, right=True,
        direction="in", labelbottom=False)

phi_ax.set_xlim(mjds[0]-100, mjds[-1]+100)
phi_ax.set_xlabel(r"Time (MJD)")
phi_ax.set_ylabel(r"$\xi$")
phi_ax.legend(loc="best")
phi_ax.tick_params(axis="both", bottom=True, top=True, left=True, right=True,
        direction="in", labelbottom=True)

plt.show()
