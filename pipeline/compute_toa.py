import numpy as np
import bilby
import glob
import sys

SEC_PER_DAY = 24*60*60

files = glob.glob("outdir/eclipse_20*/label_result.json")

temp_mjds = [58623.7354876, 58684.2675790, 58684.3698294, 58723.1226415,
    58723.2248911, 59058.1965465, 59058.2987961, 59325.6832159, 59325.7854658]

for i in range(0, len(files)):
    result = bilby.result.read_in_result(filename=files[i])

    dphi = result.posterior.delta_phi_B.values
    P0 = result.posterior.P0_B.values

    ToA = temp_mjds[i] + (np.mean(dphi * P0)/SEC_PER_DAY)
    ToA_err = np.std(dphi * P0)

    print("test 1283.58203125 {0} {1} meerkat".format(ToA, ToA_err))
