import numpy as np
import os

phase_offsets = np.linspace(-0.5, 0.5, 20)

for i in range(0, len(phase_offsets)):
    #print("sbatch phase_slurm.sh {0} {1}".format(i, phase_offsets[i]))
    os.system("sbatch phase_slurm.sh {0} {1}".format(i, phase_offsets[i]))
