import numpy as np
import argparse
import bilby
import csv
import os
import time as runtime
import matplotlib.pyplot as plt

from astropy.time import Time
from likelihood import RePairedGaussianLikelihood,SimplifiedLikelihood
from template import generate_eclipse

# Global params
SEC_PER_DAY = np.float128(24*60*60)
DEG_TO_RAD = np.float128(np.pi/180)

def get_input_arguments(parser):
    """
    Get the input arguments specified by the user.
    """
    parser.add_argument("-e1", dest="eclipse_file_1", type=str,
        help="1st input .eclipse file.")
    parser.add_argument("-e2", dest="eclipse_file_2", type=str,
        help="2nd input .eclipse file.")
    parser.add_argument("-b", dest="band", type=str,
        help="Frequency band [Lband, UHF]")
    parser.add_argument("-l", dest="label", type=str, help="Output label")
    
    return parser.parse_args()


def compute_B_period(MJD):
    return 2.77346077007 + (8.92e-16 * (MJD - 53156.0))


class eclipse_data(object):
    """
    Unpack data from an input eclipse textfile.
    """
    def __init__(self, filename):

        self.datafile = np.genfromtxt(filename, skip_header=4,
            delimiter=" ")
        self.orbital_phase = self.datafile[:,1]/360.
        #self.on_eclipse = np.transpose(np.argwhere(
            #(self.orbital_phase >= 0.247) & (self.orbital_phase <= 0.2517)))[0]
            #(self.orbital_phase >= 0.247) & (self.orbital_phase <= 0.253)))[0]
            #(self.orbital_phase >= 0.246) & (self.orbital_phase <= 0.254)))[0]

    def get_date_mjd(self):
        #return self.datafile[self.on_eclipse,0][0]
        return self.datafile[:,0][0]

    def get_time(self):
        self.date_mjd = self.get_date_mjd()
        #return (self.datafile[self.on_eclipse,0] - self.date_mjd) * SEC_PER_DAY
        return (self.datafile[:,0] - self.date_mjd) * SEC_PER_DAY

    def get_orbital_phase(self):
        #return self.orbital_phase[self.on_eclipse]
        return self.orbital_phase

    def get_flux(self):
        #self.flux = ( self.datafile[self.on_eclipse,2]
        #    /np.median(self.datafile[:,2]) )
        self.flux = ( self.datafile[:,2]
            /np.median(self.datafile[:,2]) )
        return self.flux

    def get_flux_err(self):
        #self.flux_err = ( self.datafile[self.on_eclipse,3]
        #    /np.median(self.datafile[:,2]) )
        self.flux_err = ( self.datafile[:,3]
            /np.median(self.datafile[:,2]) )
        return self.flux_err


# If run directly
if __name__ == "__main__":
    Start = runtime.time()

    parser = argparse.ArgumentParser()
    args = get_input_arguments(parser)

    #outdir="./newest_geometrix_fix"
    #outdir="./outdir/tempo1_range"
    #outdir="./outdir/tempo1_psrchive"
    #outdir="./outdir/tempo1_timing"
    #outdir="./outdir/fix_nu_nope_tempo1"
    #outdir="./outdir/fix_nu_yes_tempo1"
    outdir="./outdir/newtest"
    obs_label = args.label

    if args.band == "Lband":
        freq = 0.128358203
        #phase_offset = -1.3946877027402426e-05 
    elif args.band == "UHF":
        freq = 0.815734375
        #phase_offset = -3.453239513121366e-05
    elif args.band == "GBT820":
        freq = 0.82000
    else:
        raise ValueError("Frequency band (currently) unsupported.") 

    eclipse_1 = eclipse_data(args.eclipse_file_1)
    eclipse_2 = eclipse_data(args.eclipse_file_2)

    # Correct for DM effects:
    orb_phase_1 = eclipse_1.get_orbital_phase()# + phase_offset
    orb_phase_2 = eclipse_2.get_orbital_phase()# + phase_offset

    # Get spin period of B at eclipse date
    P0_B = compute_B_period(eclipse_1.get_date_mjd())

    # Set-up priors
    parameters = dict(
        delta_phi_B_1=None, 
        delta_phi_B_2=None, 
        P0_B=None, 
        theta=None,
        chi=None, 
        phi=None, 
        r_min=None, 
        upsilon=None, 
        z=None, 
        ratio=None, 
        sigma_1=None, 
        sigma_2=None
        )

    # plt.errorbar(orb_phase_1, eclipse_1.get_flux(), yerr=eclipse_1.get_flux_err(), fmt=".", elinewidth=1, color="tab:blue")
    # plt.plot(orb_phase_1, eclipse_1.get_flux(), lw=1, color="tab:blue")
    # plt.errorbar(orb_phase_2, eclipse_2.get_flux(), yerr=eclipse_2.get_flux_err(), fmt=".", elinewidth=1, color="tab:orange")
    # plt.plot(orb_phase_2, eclipse_2.get_flux(), lw=1, color="tab:orange")
    # plt.show()
    # plt.close()

    #quit()
    
    # Restricted Gaussian priors
    #priors = dict()
    #priors["delta_phi_B"] = bilby.core.prior.Uniform(0, 1, r"$\Delta\phi_{\rm B}$")
    #priors["P0_B"] = bilby.core.prior.DeltaFunction(P0_B, r"$P_{\rm B}$")
    #priors["P0_B"] = bilby.core.prior.Gaussian(P0_B, 0.01, r"$P_{\rm B}$")
    #priors["theta"] = bilby.core.prior.Gaussian(130, 4, r"$\theta$")
    #priors["chi"] = bilby.core.prior.Gaussian(70.9, 1, r"$\chi$")
    #priors["phi"] = bilby.core.prior.Uniform(-90, 90, r"$\phi_{\rm B}$")
    #priors["r_min"] = bilby.core.prior.Uniform(0, 1, r"$r_{\rm min}$")
    #priors["upsilon"] = bilby.core.prior.Gaussian(2, 0.2, r"$\upsilon$")
    #priors["z"] = bilby.core.prior.Gaussian(-0.543, 0.1, r"$z_{0}/R_{\rm mag}$")
    #priors["ratio"] = bilby.core.prior.Gaussian(1.29, 0.2, r"$\xi$")

    # Uniform priors (replace above with these)
    priors = dict()
    priors["delta_phi_B_1"] = bilby.core.prior.Uniform(0, 1, r"$\Delta\phi_{\rm B}$")
    priors["delta_phi_B_2"] = bilby.core.prior.Uniform(0, 1, r"$\Delta\phi_{\rm B}$")
    #priors["P0_B"] = bilby.core.prior.Gaussian(2.7734, 0.01, r"$P_{\rm B}$")
    priors["P0_B"] = bilby.core.prior.Uniform(2.6, 2.8, r"$P_{\rm B}$")
    #priors["P0_B"] = bilby.core.prior.DeltaFunction(P0_B, r"$P_{\rm B}$")
    priors["theta"] = bilby.core.prior.Uniform(90, 180, r"$\theta$")
    #priors["theta"] = bilby.core.prior.DeltaFunction(130.02, r"$\theta$")
    priors["chi"] = bilby.core.prior.Uniform(0, 90, r"$\chi$")
    #priors["chi"] = bilby.core.prior.DeltaFunction(70.94, r"$\chi$")
    priors["phi"] = bilby.core.prior.Uniform(-90, 90, r"$\phi$")
    priors["r_min"] = bilby.core.prior.Uniform(0, 1, r"$r_{\rm min}$")
    #priors["r_min"] = bilby.core.prior.DeltaFunction(0.5, r"$r_{\rm min}$")
    #priors["upsilon"] = bilby.core.prior.Uniform(0, 10, r"$\upsilon$")
    priors["upsilon"] = bilby.core.prior.DeltaFunction(2, r"$\upsilon$")
    priors["z"] = bilby.core.prior.Uniform(-1, 0, r"$z_{0}/R_{\rm mag}$")
    priors["ratio"] = bilby.core.prior.Uniform(0.5, 2.0, r"$\xi$")
    #priors["ratio"] = bilby.core.prior.DeltaFunction(1.0, r"$\xi$")
    priors["sigma_1"] = bilby.core.prior.Uniform(0, 1, r"$\sigma_{\rm noise, 1}$")
    priors["sigma_2"] = bilby.core.prior.Uniform(0, 1, r"$\sigma_{\rm noise, 2}$")

    # Experiment...
    parameters["zeta"] = None
    parameters["dorb"] = None
    priors["zeta"] = bilby.core.prior.Uniform(-3, 3, r"$\zeta$")
    #priors["zeta"] = bilby.core.prior.DeltaFunction(5./3, r"$\zeta$")
    #priors["dorb_1"] = bilby.core.prior.DeltaFunction(0.0, r"$\delta\phi_{\rm orb, 1}$")
    #priors["dorb_2"] = bilby.core.prior.DeltaFunction(0.0, r"$\delta\phi_{\rm orb, 2}$")
    priors["dorb_1"] = bilby.core.prior.Uniform(-1e-3, 1e-3, r"$\delta\phi_{\rm orb, 1}$")
    priors["dorb_2"] = bilby.core.prior.Uniform(-1e-3, 1e-3, r"$\delta\phi_{\rm orb, 2}$")

    # Define the likelihood
    # likelihood = RePairedGaussianLikelihood(
    #     time=[
    #         eclipse_1.get_time(), 
    #         eclipse_2.get_time()
    #         ], 
    #     orbital_phase=[
    #         orb_phase_1, 
    #         orb_phase_2
    #         ],
    #     flux=[
    #         eclipse_1.get_flux(), 
    #         eclipse_2.get_flux()
    #         ], 
    #     flux_err=[
    #         eclipse_1.get_flux_err(), 
    #         eclipse_2.get_flux_err()
    #         ],
    #     date=[
    #         eclipse_1.get_date_mjd(), 
    #         eclipse_2.get_date_mjd()
    #         ], 
    #     nu=freq, 
    #     parameters=parameters
    #     )

    likelihood = SimplifiedLikelihood(
        eclipse_1, 
        eclipse_2, 
        nu=freq, 
        parameters=parameters
    )

    # Run sampling with pymultinest
    results = bilby.run_sampler(priors=priors, likelihood=likelihood, 
        sampler="pymultinest", nlive=1024, evidence_tolerance=0.1,
        importance_nested_sampling=True, resume=True, 
        #outdir=outdir, label=obs_label) 
        outdir="{0}/{1}".format(outdir, obs_label))

    # Plot posterior samples with Breton+2008 results overlaid
    #injected_params = dict(delta_phi_B_1=None, delta_phi_B_2=None, theta=130.02, chi=70.94, phi=None,
    #    r_min=None, upsilon=2.0, z=-0.543, ratio=1.28, dorb_1=None, dorb_2=None, sigma_1=None, 
    #    sigma_2=None)

    results.plot_corner(filename="{0}/{1}/{1}_corner.png".format(outdir, obs_label), 
        priors=True, plot_contours=True, dpi=150)
    plt.close()

    Finish = runtime.time()
    print("Parameter estimation took {0} seconds to complete".format(Finish - Start))

    phi_B = results.posterior.delta_phi_B_1.values
    P_B = results.posterior.P0_B.values

    ToA_offset = (phi_B * P_B) / SEC_PER_DAY

    os.system("rm {0}/{1}/ToA.tim".format(outdir, obs_label))

    with open("{0}/{1}/ToA.tim".format(outdir, obs_label), "a") as toa_file:
        toa_file.write("test {0} {1} {2} bat\n".format(freq,
            eclipse_1.get_date_mjd() + np.mean(ToA_offset),
            np.std(phi_B * P_B * 1e6)))
        toa_file.close()
