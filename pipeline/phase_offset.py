import numpy as np
import argparse
import bilby
import csv
import os
import time as runtime
import matplotlib.pyplot as plt

from astropy.time import Time
from likelihood import PairedGaussianLikelihood
from template import generate_eclipse

# Global params
SEC_PER_DAY = 24*60*60
DEG_TO_RAD = np.pi/180

def get_input_arguments(parser):
    """
    Get the input arguments specified by the user.
    """
    parser.add_argument("-e1", dest="eclipse_file_1", type=str,
        help="1st input .eclipse file.")
    parser.add_argument("-e2", dest="eclipse_file_2", type=str,
        help="2nd input .eclipse file.")
    parser.add_argument("-b", dest="band", type=str,
        help="Frequency band [Lband, UHF]")
    parser.add_argument("-l", dest="label", type=str, help="Output label")
    parser.add_argument("-p", dest="phase", type=float, 
        help="Orbital-phase offset")
    
    return parser.parse_args()


def compute_B_period(MJD):
    return 2.77346077007 + (8.92e-16 * (MJD - 53156.0))


class eclipse_data(object):
    """
    Unpack data from an input eclipse textfile.
    """
    def __init__(self, filename):

        self.datafile = np.genfromtxt(filename, skip_header=4,
            delimiter=" ")
        self.orbital_phase = self.datafile[:,1]/360.
        self.on_eclipse = np.transpose(np.argwhere(
            #(self.orbital_phase >= 0.247) & (self.orbital_phase <= 0.2517)))[0]
            #(self.orbital_phase >= 0.247) & (self.orbital_phase <= 0.253)))[0]
            (self.orbital_phase >= 0.246) & (self.orbital_phase <= 0.254)))[0]

    def get_date_mjd(self):
        return self.datafile[self.on_eclipse,0][0]

    def get_time(self):
        self.date_mjd = self.get_date_mjd()
        return (self.datafile[self.on_eclipse,0] - self.date_mjd) * SEC_PER_DAY

    def get_orbital_phase(self):
        return self.orbital_phase[self.on_eclipse]

    def get_flux(self):
        self.flux = ( self.datafile[self.on_eclipse,2]
            /np.median(self.datafile[:self.on_eclipse[0],2]) )
        return self.flux

    def get_flux_err(self):
        self.flux_err = ( self.datafile[self.on_eclipse,3]
            /np.median(self.datafile[:self.on_eclipse[0],2]) )
        return self.flux_err


# If run directly
if __name__ == "__main__":
    Start = runtime.time()

    parser = argparse.ArgumentParser()
    args = get_input_arguments(parser)

    obs_label = args.label
    phase_offset = args.phase * (1.0/360.0)

    if args.band == "Lband":
        freq = 0.128358203125
    elif args.band == "UHF":
        freq = 0.815734375
    elif args.band == "GBT820":
        freq = 0.82000
    else:
        raise ValueError("Frequency band (currently) unsupported.") 

    eclipse_1 = eclipse_data(args.eclipse_file_1)
    eclipse_2 = eclipse_data(args.eclipse_file_2)

    # Correct for DM effects:
    orb_phase_1 = eclipse_1.get_orbital_phase() + phase_offset
    orb_phase_2 = eclipse_2.get_orbital_phase() + phase_offset

    # Get spin period of B at eclipse date
    P0_B = compute_B_period(eclipse_1.get_date_mjd())

    # Generate simulated eclipses
    injected_params = dict(delta_phi_B=0.5, theta=130.0, chi=70.0,
        phi=0, r_min=0.5, upsilon=2.0, z=-0.5, ratio=1.2, P0_B=P0_B,
        zeta=1.5)

    dt = (eclipse_2.get_date_mjd() - eclipse_1.get_date_mjd()) * SEC_PER_DAY 

    sim_1 = generate_eclipse(eclipse_1.get_time(), orb_phase_1, None,
        date=eclipse_1.get_date_mjd(), nu=freq, **injected_params)
    flux_1 = sim_1 + np.random.normal(0.0, 0.1, len(sim_1))

    sim_2 = generate_eclipse(eclipse_2.get_time()+dt, orb_phase_2, None, 
        date=eclipse_2.get_date_mjd(), nu=freq, **injected_params)
    flux_2 = sim_2 + np.random.normal(0.0, 0.1, len(sim_2))

    #plt.plot(orb_phase_1, flux_1)
    #plt.plot(orb_phase_1 - phase_offset, flux_1)
    #plt.show()
    #quit()

    # Set-up priors
    parameters = dict(delta_phi_B=None, P0_B=None, theta=None, chi=None, 
        phi=None, r_min=None, upsilon=None, z=None, ratio=None, sigma_1=None, 
        sigma_2=None)

    # Restricted Gaussian priors
    priors = dict()
    priors["delta_phi_B"] = bilby.core.prior.Uniform(0, 1, r"$\Delta\phi_{\rm B}$")
    priors["P0_B"] = bilby.core.prior.DeltaFunction(P0_B, r"$P_{\rm B}$")
    priors["theta"] = bilby.core.prior.Gaussian(130, 4, r"$\theta$")
    priors["chi"] = bilby.core.prior.Gaussian(71, 1, r"$\chi$")
    priors["phi"] = bilby.core.prior.Uniform(-90, 90, r"$\phi_{\rm B}$")
    priors["r_min"] = bilby.core.prior.Uniform(0, 1, r"$r_{\rm min}$")
    priors["upsilon"] = bilby.core.prior.Gaussian(2, 0.2, r"$\upsilon$")
    priors["z"] = bilby.core.prior.Gaussian(-0.5, 0.1, r"$z_{0}/R_{\rm mag}$")
    priors["ratio"] = bilby.core.prior.Gaussian(1.0, 0.2, r"$\xi$")
    priors["zeta"] = bilby.core.prior.Uniform(0.1, 5, r"$\zeta$")
    priors["sigma_1"] = bilby.core.prior.Uniform(0, 2, r"$\sigma_{\rm noise, 1}$")
    priors["sigma_2"] = bilby.core.prior.Uniform(0, 2, r"$\sigma_{\rm noise, 2}$")

    # Define the likelihood
    likelihood = PairedGaussianLikelihood(
        time=[eclipse_1.get_time(), eclipse_2.get_time()], 
        orbital_phase=[orb_phase_1, orb_phase_2],
        flux=[flux_1, flux_2], 
        flux_err=[eclipse_1.get_flux_err(), eclipse_2.get_flux_err()],
        date=[eclipse_1.get_date_mjd(), eclipse_2.get_date_mjd()], 
        nu=freq, parameters=parameters)

    # Run sampling with pymultinest
    results = bilby.run_sampler(priors=priors, likelihood=likelihood, 
        sampler="pymultinest", walks=50, nlive=512, evidence_tolerance=0.1,
        importance_nested_sampling=True, resume=True, 
        #outdir=outdir, label=obs_label) 
        outdir="./outdir/finding_the_problem/test_{0}".format(obs_label))

    results.plot_corner(
        filename="./outdir/finding_the_problem/test_{0}/{0}_corner.png".format(obs_label),
        truth=injected_params, priors=True, plot_contours=True, dpi=150)
    plt.close()

    Finish = runtime.time()
    print("Parameter estimation took {0} seconds to complete".format(Finish - Start))
