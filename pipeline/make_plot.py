import numpy as np
import argparse
import random
import bilby
import matplotlib.pyplot as plt
from utils import get_rc_params
from paired_run_pipeline import compute_B_period,eclipse_data
from template import generate_eclipse

# Global params
SEC_PER_DAY = 24*60*60
DEG_TO_RAD = np.pi/180


def get_input_arguments(parser):
    """
    Get the input arguments specified by the user.
    """
    parser.add_argument("-e1", dest="eclipse_file_1", type=str,
        help="1st input .eclipse file.")
    parser.add_argument("-e2", dest="eclipse_file_2", type=str,
        help="2nd input .eclipse file.")
    parser.add_argument("-e3", dest="eclipse_file_3", type=str,
        help="3rd input .eclipse file.")
    parser.add_argument("-e4", dest="eclipse_file_4", type=str,
        help="4th input .eclipse file.")
    parser.add_argument("-b", dest="band", type=str,
        help="Frequency band [Lband, UHF]")
    parser.add_argument("-l1", dest="label_1", type=str, help="Output label 1")
    parser.add_argument("-l2", dest="label_2", type=str, help="Output label 2")

    return parser.parse_args()


# If run directly
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    args = get_input_arguments(parser)

    outdir = "./outdir/new_with_offset"
    obs_label_1 = args.label_1
    obs_label_2 = args.label_2

    freq_2 = 1.28446
    freq_1 = 0.81573

    eclipse_1 = eclipse_data(args.eclipse_file_1)
    eclipse_2 = eclipse_data(args.eclipse_file_2)
    eclipse_3 = eclipse_data(args.eclipse_file_3)
    eclipse_4 = eclipse_data(args.eclipse_file_4)

    dt_1 = (eclipse_2.get_date_mjd() - eclipse_1.get_date_mjd()) * SEC_PER_DAY
    dt_2 = (eclipse_4.get_date_mjd() - eclipse_3.get_date_mjd()) * SEC_PER_DAY

    # Get spin period of B at eclipse date
    P0_B1 = compute_B_period(eclipse_1.get_date_mjd())
    P0_B2 = compute_B_period(eclipse_1.get_date_mjd())

    # Get posterior samples
    results_1 = bilby.core.result.read_in_result("{0}/{1}/label_result.json".format(outdir,
        obs_label_1))
    delta_phi_B1_1 = random.sample(results_1.posterior.delta_phi_B_1.to_list(), 100) 
    delta_phi_B2_1 = random.sample(results_1.posterior.delta_phi_B_2.to_list(), 100)
    theta1 = random.sample(results_1.posterior.theta.to_list(), 100)
    chi1 = random.sample(results_1.posterior.chi.to_list(), 100)
    phi1 = random.sample(results_1.posterior.phi.to_list(), 100)
    r_min1 = random.sample(results_1.posterior.r_min.to_list(), 100)
    upsilon1 = random.sample(results_1.posterior.upsilon.to_list(), 100)
    z1 = random.sample(results_1.posterior.z.to_list(), 100)
    ratio1 = random.sample(results_1.posterior.ratio.to_list(), 100)
    #zeta2 = random.sample(results_1.posterior.zeta.to_list(), 100)

    results_2 = bilby.core.result.read_in_result("{0}/{1}/label_result.json".format(outdir,
        obs_label_2))
    delta_phi_B1_2 = random.sample(results_1.posterior.delta_phi_B_1.to_list(), 100) 
    delta_phi_B2_2 = random.sample(results_1.posterior.delta_phi_B_2.to_list(), 100)
    theta2 = random.sample(results_1.posterior.theta.to_list(), 100)
    chi2 = random.sample(results_1.posterior.chi.to_list(), 100)
    phi2 = random.sample(results_1.posterior.phi.to_list(), 100)
    r_min2 = random.sample(results_1.posterior.r_min.to_list(), 100)
    upsilon2 = random.sample(results_1.posterior.upsilon.to_list(), 100)
    z2 = random.sample(results_1.posterior.z.to_list(), 100)
    ratio2 = random.sample(results_1.posterior.ratio.to_list(), 100)
    #zeta2 = random.sample(results_1.posterior.zeta.to_list(), 100)
    
    # Median template for both eclipse pairs
    medians1 = dict(delta_phi_B=np.median(delta_phi_B1_1), theta=np.median(theta1),
        chi=np.median(chi1), phi=np.median(phi1), r_min=np.median(r_min1),
        upsilon=np.median(upsilon1), z=np.median(z1), ratio=np.median(ratio1), 
        P0_B=P0_B1)

    template_1 = generate_eclipse(eclipse_1.get_time(), eclipse_1.get_orbital_phase(),
        eclipse_1.get_flux(), date=eclipse_1.get_date_mjd(), nu=freq_1, **medians1)

    medians1 = dict(delta_phi_B=np.median(delta_phi_B2_1), theta=np.median(theta1),
        chi=np.median(chi1), phi=np.median(phi1), r_min=np.median(r_min1),
        upsilon=np.median(upsilon1), z=np.median(z1), ratio=np.median(ratio1),
        P0_B=P0_B1)

    template_2 = generate_eclipse(eclipse_2.get_time(), eclipse_2.get_orbital_phase(),
       eclipse_2.get_flux(), date=eclipse_2.get_date_mjd(), nu=freq_1, **medians1)

    medians2 = dict(delta_phi_B=np.median(delta_phi_B1_2), theta=np.median(theta2),
        chi=np.median(chi2), phi=np.median(phi2), r_min=np.median(r_min2),
        upsilon=np.median(upsilon2), z=np.median(z2), ratio=np.median(ratio2),
        P0_B=P0_B2)

    template_3 = generate_eclipse(eclipse_3.get_time(), eclipse_3.get_orbital_phase(),
        eclipse_3.get_flux(), date=eclipse_3.get_date_mjd(), nu=freq_2, **medians2)

    medians2 = dict(delta_phi_B=np.median(delta_phi_B2_2), theta=np.median(theta2),
        chi=np.median(chi2), phi=np.median(phi2), r_min=np.median(r_min2),
        upsilon=np.median(upsilon2), z=np.median(z2), ratio=np.median(ratio2),
        P0_B=P0_B2)

    template_4 = generate_eclipse(eclipse_4.get_time()+dt_2, eclipse_4.get_orbital_phase(),
       eclipse_4.get_flux(), date=eclipse_4.get_date_mjd(), nu=freq_2, **medians2)
    

    plt.rcParams.update(get_rc_params())
    fig = plt.figure(figsize=(10,8))
    grid = plt.GridSpec(8, 8, hspace=0.1, wspace=0.1)
    
    el1_ax = fig.add_subplot(grid[0:4,0:4])
    el2_ax = fig.add_subplot(grid[0:4,4:8], sharey=el1_ax)
    eu1_ax = fig.add_subplot(grid[4:8,0:4], sharex=el1_ax)
    eu2_ax = fig.add_subplot(grid[4:8,4:8], sharex=el2_ax, sharey=eu1_ax)

    for i in range(0, 100):
        medians1 = dict(delta_phi_B=delta_phi_B1_1[i], theta=theta1[i],
            chi=chi1[i], phi=phi1[i], r_min=r_min1[i], upsilon=upsilon1[i], 
            z=z1[i], ratio=ratio1[i], P0_B=P0_B1)
        tmp_1 = generate_eclipse(eclipse_1.get_time(), eclipse_1.get_orbital_phase(),
            eclipse_1.get_flux(), date=eclipse_1.get_date_mjd(), nu=freq_1, **medians1)

        medians1 = dict(delta_phi_B=delta_phi_B2_1[i], theta=theta1[i],
            chi=chi1[i], phi=phi1[i], r_min=r_min1[i], upsilon=upsilon1[i],
            z=z1[i], ratio=ratio1[i], P0_B=P0_B1)
        tmp_2 = generate_eclipse(eclipse_2.get_time()+dt_1, eclipse_2.get_orbital_phase(),
            eclipse_2.get_flux(), date=eclipse_2.get_date_mjd(), nu=freq_1, **medians1)

        medians2 = dict(delta_phi_B=delta_phi_B1_2[i], theta=theta2[i], 
            chi=chi2[i], phi=phi2[i], r_min=r_min2[i], upsilon=upsilon2[i],
            z=z2[i], ratio=ratio2[i], P0_B=P0_B2)
        tmp_3 = generate_eclipse(eclipse_3.get_time(), eclipse_3.get_orbital_phase(),
            eclipse_3.get_flux(), date=eclipse_3.get_date_mjd(), nu=freq_2, **medians2)

        medians2 = dict(delta_phi_B=delta_phi_B2_2[i], theta=theta2[i],
            chi=chi2[i], phi=phi2[i], r_min=r_min2[i], upsilon=upsilon2[i],
            z=z2[i], ratio=ratio2[i], P0_B=P0_B2)
        tmp_4 = generate_eclipse(eclipse_4.get_time()+dt_2, eclipse_4.get_orbital_phase(),
            eclipse_4.get_flux(), date=eclipse_4.get_date_mjd(), nu=freq_2, **medians2)

        el1_ax.plot(eclipse_1.get_orbital_phase(), tmp_1, color="tab:orange", lw=1, alpha=0.2)
        el2_ax.plot(eclipse_2.get_orbital_phase(), tmp_2, color="tab:orange", lw=1, alpha=0.2)
        eu1_ax.plot(eclipse_3.get_orbital_phase(), tmp_3, color="tab:orange", lw=1, alpha=0.2)
        eu2_ax.plot(eclipse_4.get_orbital_phase(), tmp_4, color="tab:orange", lw=1, alpha=0.2)

    el1_ax.errorbar(eclipse_1.get_orbital_phase(), eclipse_1.get_flux(), yerr=eclipse_1.get_flux_err(), 
        color="k", ecolor="tab:grey", lw=1)
    el1_ax.plot(eclipse_1.get_orbital_phase(), template_1, color="#7570b3", lw=1.5)
    el1_ax.set_xlim(eclipse_1.get_orbital_phase()[0], eclipse_1.get_orbital_phase()[-1])
    el1_ax.set_ylim(-0.4,1.8)
    el1_ax.set_ylabel("Normalised flux")
    el1_ax.text(0.05, 0.9, "2021-04-21-16:17:32  1248 MHz", transform=el1_ax.transAxes, fontsize=12)
    el1_ax.tick_params(axis="both", direction="in", labelbottom=False)

    el2_ax.errorbar(eclipse_2.get_orbital_phase(), eclipse_2.get_flux(), yerr=eclipse_2.get_flux_err(),
        color="k", ecolor="tab:grey", lw=1)
    el2_ax.plot(eclipse_2.get_orbital_phase(), template_2, color="#7570b3", lw=1.5)
    el2_ax.set_xlim(eclipse_2.get_orbital_phase()[0], eclipse_2.get_orbital_phase()[-1])
    el2_ax.set_ylim(-0.4,1.8)
    el2_ax.text(0.05, 0.9, "2021-04-21-18:47:37  1248 MHz", transform=el2_ax.transAxes, fontsize=12)
    el2_ax.tick_params(axis="both", direction="in", labelbottom=False, labelleft=False)

    eu1_ax.errorbar(eclipse_3.get_orbital_phase(), eclipse_3.get_flux(), yerr=eclipse_3.get_flux_err(),
        color="k", ecolor="tab:grey", lw=1)
    eu1_ax.plot(eclipse_3.get_orbital_phase(), template_3, color="#7570b3", lw=1.5)
    eu1_ax.set_xlim(eclipse_3.get_orbital_phase()[0], eclipse_1.get_orbital_phase()[-1])
    eu1_ax.set_ylim(-0.4,1.8)
    eu1_ax.set_ylabel("Normalised flux")
    eu1_ax.set_xlabel("Orbital phase (deg)")
    eu1_ax.text(0.05, 0.9, "2021-03-19-13:00:53  816 MHz", transform=eu1_ax.transAxes, fontsize=12)
    eu1_ax.tick_params(axis="both", direction="in", labelbottom=True)

    eu2_ax.errorbar(eclipse_4.get_orbital_phase(), eclipse_4.get_flux(), yerr=eclipse_4.get_flux_err(),
        color="k", ecolor="tab:grey", lw=1)
    eu2_ax.plot(eclipse_4.get_orbital_phase(), template_4, color="#7570b3", lw=1.5)
    eu2_ax.set_xlim(eclipse_4.get_orbital_phase()[0], eclipse_4.get_orbital_phase()[-1])
    eu2_ax.set_ylim(-0.4,1.8)
    eu2_ax.set_xlabel("Orbital phase (deg)")
    eu2_ax.text(0.05, 0.9, "2021-03-19-15:30:57  816 MHz", transform=eu2_ax.transAxes, fontsize=12)
    eu2_ax.tick_params(axis="both", direction="in", labelbottom=True, labelleft=False)

    plt.savefig("../eclipse_fits.pdf")
    plt.show()
