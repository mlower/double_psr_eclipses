#!/bin/bash
#SBATCH --job-name=eclipse_test
#SBATCH --ntasks=1
#SBATCH --time=00-09:00:00
#SBATCH --mem-per-cpu=2G

DIR=/fred/oz005/users/mlower/double_psr/double_psr_eclipses
DATADIR=/fred/oz005/users/mlower/double_psr/eclipses
LABEL=$1
PHASE=$2

source /fred/oz005/users/mlower/double_psr/double_psr_eclipses/ozstar_setup.sh

srun python phase_offset.py -e1 ../../eclipses/2021-03-19-13\:00\:53/2021-03-19-13\:00\:53.eclipse -e2 ../../eclipses/2021-03-19-15\:30\:57/2021-03-19-15\:30\:57.eclipse -b UHF -l ${LABEL} -p ${PHASE}

exit 
