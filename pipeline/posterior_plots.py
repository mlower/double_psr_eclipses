import numpy as np
import bilby
import matplotlib.pyplot as plt

import scipy.stats as st
from scipy.stats import gaussian_kde
from utils import get_rc_params,get_median_and_bounds

def return_kde(samples, param_range):
    """
    Returns kernal density estimate of the input 1-D posterior
    samples.
    """
    kde = gaussian_kde(samples)
    kde_norm = kde.evaluate(param_range)/np.sum(
        kde.evaluate(param_range))

    return kde_norm


dates_lband = np.genfromtxt("./lists/uhf.list", dtype=str)
dir = "new_with_offset"

mjds_lband = []
phi_lband = []
alpha_lband = []
theta_lband = []
for i in range(0, len(dates_lband)):
    toa_file = np.genfromtxt("./outdir/{0}/{1}/ToA.tim".format(
        dir, dates_lband[i]), dtype=float)
    mjds_lband.append(toa_file[2])

    result_file = "./outdir/{0}/{1}/label_result.json".format(
        dir, dates_lband[i])
    result = bilby.result.read_in_result(result_file)
    alpha_lband.append(result.posterior.chi.values)# - 3.6)
    theta_lband.append(result.posterior.theta.values)# - 12.6)


amin, amax = 45, 70
tmin, tmax = 135, 150

# Peform the kernel density estimate
aa, tt = np.mgrid[amin:amax:1000j, tmin:tmax:1000j]
positions = np.vstack([aa.ravel(), tt.ravel()])

f = np.ones((1000, 1000))
for i in range(0, len(dates_lband)):
    values = np.vstack([alpha_lband[i], theta_lband[i]])
    kernel = st.gaussian_kde(values)
    f = f * np.reshape(kernel(positions).T, aa.shape)

fig = plt.figure()
ax = fig.gca()
ax.set_xlim(amin, amax)
ax.set_ylim(tmin, tmax)
# Contourf plot
cfset = ax.contourf(aa, tt, f, cmap='Blues')
## Or kernel density estimate plot instead of the contourf plot
# Contour plot
cset = ax.contour(aa, tt, f, colors='k')
# Label plot
ax.clabel(cset, inline=1, fontsize=10)
ax.set_xlabel('alpha')
ax.set_ylabel('theta')

plt.show()
plt.close()
quit()

alpha_range = np.linspace(45, 70, 1024)
combine_alpha = np.ones((1024))
for i in range(0, len(dates_lband)):
    alpha_KDE = return_kde(alpha_lband[i], alpha_range)
    combine_alpha *= alpha_KDE

    plt.plot(alpha_range, alpha_KDE, color="tab:grey", alpha=0.6)
plt.plot(alpha_range, combine_alpha/sum(combine_alpha))
plt.show()
plt.close()

alpha_samp = np.random.choice(alpha_range, p=combine_alpha/np.sum(combine_alpha), size=2048)
print(get_median_and_bounds(alpha_samp))

theta_range = np.linspace(130, 150, 1024)
combine_theta = np.ones((1024))
for i in range(0, len(dates_lband)):
    theta_KDE = return_kde(theta_lband[i], theta_range)
    combine_theta *= theta_KDE

    plt.plot(theta_range, theta_KDE, color="tab:grey", alpha=0.6)
plt.plot(theta_range, combine_theta/sum(combine_theta))
plt.show()
plt.close()

theta_samp = np.random.choice(theta_range, p=combine_theta/np.sum(combine_theta), size=2048)
print(get_median_and_bounds(theta_samp))

quit()

H = np.ones((64, 64))
for i in range(0, len(dates_lband)):
    H_new, xe, ye = np.histogram2d(alpha_lband[0], theta_lband[0], bins=64, normed=True)
    
    H = H * H_new

plt.imshow(H, aspect="auto", extent=[xe[0], xe[-1], ye[0], ye[-1]])
plt.show()
plt.close()
