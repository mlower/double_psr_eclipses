#!/bin/bash

DIR=/fred/oz005/users/mlower/double_psr/double_psr_eclipses
DATADIR=/fred/oz005/users/mlower/double_psr/eclipses
LIST=`cat dates.list`

for OBS in ${LIST}; do
    python plot_eclipse.py -e ${DATADIR}/${OBS}/${OBS}.eclipse -b Lband -l ${OBS}
done
exit
