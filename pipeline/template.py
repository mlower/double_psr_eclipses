import numpy as np
import matplotlib.pyplot as plt
from astropy.time import Time
from Eclipse0737 import EclipseDipole 

# Global params
SEC_PER_DAY = 24*60*60
DEG_TO_RAD = np.pi/180

def update_P_orb(MJD):
    """
    Update orbital period using values from the latest PSR J0737-3039A 
    ephemeris (Kramer et al. submitted)

    Parameters
    ----------
    MJD : float
        MJD of the observation

    Returns
    -------
        PBDOT corrected orbital period
    """
    PEPOCH = 55700.0 # reference epoch
    PB = np.float128(0.10225155929730545) * SEC_PER_DAY # orbital period in sec
    PBDOT = np.float128(-1.247920095372992075e-12) # orbital period decay rate

    return PB + (PBDOT * (MJD - PEPOCH))    


def generate_eclipse(time, orbital_phase, flux, date=55700.0, nu=1.0, **kwargs):
    """
    Generate eclipse
    
    Return an eclipse lightcurve based on the input parameters.
    
    Parameters
    ----------
    orbital_phase : ndarray
        Orbital phase corresponding to each point in the lightcurve.
    flux : ndarray, None
        Flux for each point in the lightcurve.
        If not provided, the array will be filled with zeros, which can be
        useful for testing and theoretical lightcurve calculations.
    date : float
        The date the observation was taken in MJD.
        Default: 55700
    kwargs: dict
        Optional keyword arguments
        Supported arguments:
            P_orb : float
                Orbital period (fractional days).
                Default: 0.10225156246204
            P0_B : float
                Spin-period of J0737-3039B (seconds).
                Default: 1/0.36056035506
            n : int
                Number of orbital phase bins.
                Default: 1024
            theta : float
                Angle between the spin axis and the orbital angular momentum
                (radians).
                Default: 130.2 degrees
            chi : float
                Angle between the spin axis and the magnetic axis (radians).
                Default: 70.98 degrees
            nu : float
                Radio frequency of the observation (GHz).
                Default: 1.0 GHz
            r_min : float
                Minimum radius for synchrotron absorption (mainly due to cooling).
                Default: 0.35
            upsilon : float
                Optical depth parameter (function of electron temperature and
                density).
                Default: 2.0
            z : float
                Impact parameter, i.e. relative distance between pulsar A and B at
                their conjunction time. This can be used to infer the inclination.
                Default: -0.5
            ratio : float
                Mapping of the orbital phase to the unit coordinate system.
                Default: 1.0
            shift_phase_B : float
                Systematic phase shift to apply to the spin phase of pulsar B.
                Note: The phase is between 0-1, not 0-TWOPI.
                Default: 0.0
            do_phase : int
                If equal to 1 (default value), will calculate the shift to apply to
                compensate for the coordinate system rotation when assigning the
                fiducial spin phase point.
                Note: A value of -1 can be used to subtract the phase shift rather
                than adding it. This is wrong, but permitted for debugging.
                Default: 1
            zeta : float
                Scaling index to account for non-standard frequency evolution.
    """
    
    eclipse_kwargs = dict(
        P0_B=1/0.36056035506, theta=130.02, chi=70.98, nu=1.0, r_min=0.35,
        upsilon=2.0, z=-0.5, ratio=1.0, delta_phi_B=0.0, phi=51.21, do_phase=1,
        zeta=1.0)
    eclipse_kwargs.update(kwargs)
    
    # Pre-corrected rotational phase of J0737B throughout the eclipse
    phase_B = time / eclipse_kwargs["P0_B"]

    # Initialise the eclipse object
    eclipse = EclipseDipole(phase_B, orbital_phase, flux)
    
    Phi = eclipse_kwargs["phi"] 
    
    parameters = {
    "theta": DEG_TO_RAD * eclipse_kwargs["theta"],
    "phi": DEG_TO_RAD * Phi,
    "chi": DEG_TO_RAD * eclipse_kwargs["chi"],
    "nu": nu,
    "rmin": eclipse_kwargs["r_min"],
    "upsilon": eclipse_kwargs["upsilon"],
    "z": eclipse_kwargs["z"],
    "ratio": eclipse_kwargs["ratio"],
    "shift_phsB": eclipse_kwargs["delta_phi_B"],
    "do_phase": eclipse_kwargs["do_phase"],
    "zeta": eclipse_kwargs["zeta"]
    }
    
    return eclipse.Model(**parameters)
