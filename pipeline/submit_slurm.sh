#!/bin/bash
#SBATCH --job-name=eclipse_test
#SBATCH --ntasks=1
#SBATCH --time=00-36:00:00
#SBATCH --mem-per-cpu=2G

DIR=/fred/oz005/users/mlower/double_psr/double_psr_eclipses
DATADIR=/fred/oz005/users/mlower/double_psr/eclipses
#OBS=$1
#BAND=$2
OBS1=$1
OBS2=$2
BAND=$3
LABEL=$4

source /fred/oz005/users/mlower/double_psr/double_psr_eclipses/ozstar_setup.sh

#srun /apps/skylake/software/compiler/gcc/6.4.0/python/3.6.4/bin/python test_pipeline.py

#srun /apps/skylake/software/compiler/gcc/6.4.0/python/3.6.4/bin/python run_pipeline.py -e ${DIR}/../eclipses/2020-07-28-04\:33\:10/2020-07-28-04\:33\:10.eclipse -b Lband -o ${DIR}/real_test/another_test/ -l still_testing

#srun /apps/skylake/software/compiler/gcc/6.4.0/python/3.6.4/bin/python run_pipeline.py -e ../../eclipses/2020-07-28-04\:33\:10/2020-07-28-04\:33\:10.eclipse -b Lband -o ../real_test/cut_eclipse -l still_testing

#srun /apps/skylake/software/compiler/gcc/6.4.0/python/3.6.4/bin/python run_pipeline.py -e ${DATADIR}/${OBS}/${OBS}_uncorr.eclipse -b ${BAND} -l ${OBS}

#/apps/skylake/software/compiler/gcc/6.4.0/python/3.6.4/bin/python run_pipeline.py -e ${DATADIR}/${OBS}/${OBS}.eclipse -b Lband -l ${OBS}

srun python paired_run_pipeline.py -e1 ${DATADIR}/${OBS1}/${OBS1}.eclipse -e2 ${DATADIR}/${OBS2}/${OBS2}.eclipse -b ${BAND} -l ${LABEL}

#srun python paired_run_pipeline.py -e1 ${DATADIR}/${OBS1}/${OBS1}_new.eclipse -e2 ${DATADIR}/${OBS2}/${OBS2}_new.eclipse -b ${BAND} -l ${LABEL}

#srun python paired_run_pipeline.py -e1 ${DATADIR}/${OBS1}/${OBS1}_uncorr.eclipse -e2 ${DATADIR}/${OBS2}/${OBS2}_uncorr.eclipse -b ${BAND} -l uncorr_${LABEL}

#python paired_run_pipeline.py -e1 ${DATADIR}/${OBS1}/${OBS1}.eclipse -e2 ${DATADIR}/${OBS2}/${OBS2}.eclipse -b UHF -l ${LABEL}

exit 
