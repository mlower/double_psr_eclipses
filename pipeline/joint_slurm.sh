#!/bin/bash
#SBATCH --job-name=joint_eclipse
#SBATCH --ntasks=1
#SBATCH --time=00-24:00:00
#SBATCH --mem-per-cpu=1G

source /fred/oz005/users/mlower/double_psr/double_psr_eclipses/ozstar_setup.sh
srun /apps/skylake/software/compiler/gcc/6.4.0/python/3.6.4/bin/python coup_de_grace.py
exit 
