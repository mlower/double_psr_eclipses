import numpy as np
import bilby
import time
import sys
import matplotlib.pyplot as plt

from astropy.time import Time
from likelihood import TailoredGaussianLikelihood
from template import generate_eclipse

Start = time.time()

# Global params
SEC_PER_DAY = 24*60*60
DEG_TO_RAD = np.pi/180

eclipse = str(sys.argv[1])

datafile = np.genfromtxt("../../eclipses/{0}/{0}.eclipse".format(eclipse),
    skip_header=4, delimiter=" ")

date_mjd = datafile[0,0]
orbital_phase = datafile[:,1]/360.

orbital_phase = datafile[:,1]/360.
on_eclipse = np.transpose(np.argwhere(
    (orbital_phase >= 0.247) & (orbital_phase <= 0.2517)))[0]

#time = (datafile[on_eclipse,0] - datafile[on_eclipse,0][0]) * SEC_PER_DAY

#orbital_phase = orbital_phase[on_eclipse]# - 2.06e-5
#flux = datafile[on_eclipse[0]:on_eclipse[-1]+1,2]/np.median(datafile[:on_eclipse[0],2])
#flux_err = datafile[on_eclipse[0]:on_eclipse[-1]+1,3]/np.median(datafile[:on_eclipse[0],2])

time = (datafile[:,0] - datafile[:,0][0]) * SEC_PER_DAY
flux = datafile[:,2]/np.median(datafile[:on_eclipse[0],2])
flux_err = datafile[:,3]/np.median(datafile[:on_eclipse[0],2])

# 2021-04-21-18:47:37
kwargs = dict(delta_phi_B=0.5, theta=130.02, chi=70.94, phi=-14.23, r_min=0.5,
    upsilon=2.0, z=-0.545, ratio=1.29, P0_B=2.77346077007, zeta=5.0/3)

#0.81573
template1 = generate_eclipse(time, orbital_phase, None, date=date_mjd, nu=1.428, **kwargs)
kwargs["zeta"] = -1.5
template2 = generate_eclipse(time, orbital_phase, None, date=date_mjd, nu=1.428, **kwargs)

#flux = template + np.random.normal(0.0, 0.10, len(template))

# Plot the simulated data
#plt.errorbar(orbital_phase, flux, yerr=flux_err, fmt="-", color="k", ecolor="tab:grey")
#plt.plot(orbital_phase, flux, lw=1, color="k")
plt.plot(orbital_phase * 360, template1, lw=1, color="tab:blue")
plt.plot(orbital_phase * 360, template2, lw=1, color="tab:orange")
#plt.plot(orbital_phase + 2.06e-5, template, lw=2)
#plt.plot(orbital_phase + 2.06e-4, template, lw=2)
plt.xlim(88, 92)
plt.ylim(-0.6, 2.1)
plt.show()
quit()

# Set-up priors
parameters = dict(delta_phi_B=None, P0_B=None, theta=None, chi=None, 
    phi=None, r_min=None, upsilon=None, z=None, ratio=None, sigma=None)

priors = dict()
priors["delta_phi_B"] = bilby.core.prior.Uniform(0, 1, r"$\Delta\phi_{\rm B}$")
priors["P0_B"] = bilby.core.prior.DeltaFunction(2.77346122494, r"$P_{\rm B}$")
priors["theta"] = bilby.core.prior.Gaussian(130, 4, r"$\theta$ (deg)")
priors["chi"] = bilby.core.prior.Gaussian(71, 1, r"$\alpha$ (deg)")
priors["phi"] = bilby.core.prior.Uniform(-90, 90, r"$\varphi_{\rm B}$ (deg)")
priors["r_min"] = bilby.core.prior.Uniform(0, 1, r"$r_{\rm min}$")
priors["upsilon"] = bilby.core.prior.Gaussian(2, 0.2, r"$\upsilon$")
priors["z"] = bilby.core.prior.Gaussian(-0.5, 0.1, r"$z_{0}/R_{\rm mag}$")
priors["ratio"] = bilby.core.prior.Gaussian(1.0, 0.2, r"$\xi$")
priors["sigma"] = bilby.core.prior.Uniform(0, 2, r"$\sigma_{\rm noise}$")

# Define the likelihood for model 1
likelihood = TailoredGaussianLikelihood(time=time, orbital_phase=orbital_phase, flux=flux,
    flux_err=flux_err, date="2020-07-28T04:45:33.8544", nu=1.28446,
    parameters=parameters)

# Run sampling with dynesty
results = bilby.run_sampler(priors=priors, likelihood=likelihood, sampler="pymultinest", nlive=1024,
                            outdir="./outdir/test", label="no_offset", 
                            evidence_tolerance=0.1, importance_nested_sampling=True, resume=True)

injected_params = dict(delta_phi_B=0.75, theta=130.97, chi=70.73,
    phi=-12.95, r_min=0.5, upsilon=2.0, z=-0.5, ratio=1.0, sigma=None)

results.plot_corner(truth=injected_params, priors=True, plot_contours=False)
plt.close()
