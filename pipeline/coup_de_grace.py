import numpy as np
import bilby
import csv
import time as runtime
import matplotlib.pyplot as plt

from astropy.time import Time
from likelihood import JointGaussianLikelihood
from template import generate_eclipse

# Global params
SEC_PER_DAY = 24*60*60
DEG_TO_RAD = np.pi/180

class eclipse_data(object):
    """
    Unpack data from an input eclipse textfile.
    """
    def __init__(self, filename):

        self.datafile = np.genfromtxt(filename, skip_header=4,
            delimiter=" ")
        self.orbital_phase = self.datafile[:,1]/360.
        self.on_eclipse = np.transpose(np.argwhere(
            #(self.orbital_phase >= 0.247) & (self.orbital_phase <= 0.2517)))[0]
            (self.orbital_phase >= 0.247) & (self.orbital_phase <= 0.253)))[0]

    def get_date_mjd(self):
        return self.datafile[self.on_eclipse,0][0]

    def get_time(self):
        self.date_mjd = self.get_date_mjd()
        return (self.datafile[self.on_eclipse,0] - self.date_mjd) * SEC_PER_DAY

    def get_orbital_phase(self):
        return self.orbital_phase[self.on_eclipse]

    def get_flux(self):
        self.flux = ( self.datafile[self.on_eclipse,2]
            /np.median(self.datafile[:self.on_eclipse[0],2]) )
        return self.flux

    def get_flux_err(self):
        self.flux_err = ( self.datafile[self.on_eclipse,3]
            /np.median(self.datafile[:self.on_eclipse[0],2]) )
        return self.flux_err


# If run directly
if __name__ == "__main__":
    Start = runtime.time()

    outdir = "./outdir"
    label = "coup_de_grace"

    # Get spin period of B at eclipse date
    P0_B = 2.77346077007

    # Set-up priors
    parameters = dict(P0_B=None, theta=None, chi=None, phi=None, r_min=None,
    upsilon=None, z=None, ratio=None, sigma=None)

    # Restricted Gaussian priors
    priors = dict()
    priors["P0_B"] = bilby.core.prior.DeltaFunction(P0_B, r"$P_{\rm B}$")
    #priors["P0_B"] = bilby.core.prior.Gaussian(P0_B, 0.01, r"$P_{\rm B}$")
    priors["theta"] = bilby.core.prior.Gaussian(130, 4, r"$\theta$")
    priors["chi"] = bilby.core.prior.Gaussian(71, 1, r"$\chi$")
    priors["phi_0"] = bilby.core.prior.Uniform(-90, 90, r"$\phi_{0}$")
    priors["Omega_SO"] = bilby.core.prior.Uniform(0, 10, r"$\Omega_{\rm SO}$")
    priors["r_min"] = bilby.core.prior.Uniform(0, 1, r"$r_{\rm min}$")
    priors["upsilon"] = bilby.core.prior.Gaussian(2, 0.2, r"$\upsilon$")
    priors["z"] = bilby.core.prior.Gaussian(-0.5, 0.1, r"$z_{0}/R_{\rm mag}$")
    priors["ratio"] = bilby.core.prior.Gaussian(1.0, 0.2, r"$\xi$")
    priors["sigma"] = bilby.core.prior.DeltaFunction(0, r"$\sigma_{\rm noise}$")    
    priors["zeta"] = bilby.core.prior.Uniform(-5, 5, r"$\zeta$")

    dates, bands = np.genfromtxt("./lists/tmp_dates_n_freqs.list", dtype=str, 
        delimiter=" ", unpack=True)

    freqs = []
    mjds = []
    times = dict()
    phases = dict()
    fluxes = dict()
    errors = dict()
    for i in range(0, len(dates)):

        param_str = "delta_phi_B_{0}".format(i)
        param_lab = "dphi_{0}".format(i)
        parameters[param_str] = None
        priors[param_str] = bilby.core.prior.Uniform(0, 1, param_lab)

        if bands[i] == "Lband":
            freqs.append(1.28446)
        elif bands[i] == "UHF":
            freqs.append(0.81573)
        else:
            raise ValueError("Frequency band (currently) unsupported.") 

        elabel = "eclipse_{0}".format(i)

        eclipse_file = "../../eclipses/{0}/{0}_uncorr.eclipse".format(dates[i])
        #eclipse_file = "../../eclipses/{0}/{0}.eclipse".format(dates[i])
        eclipse = eclipse_data(eclipse_file)

        mjds.append(eclipse.get_date_mjd())
        times[elabel] = eclipse.get_time()
        phases[elabel] = eclipse.get_orbital_phase()
        fluxes[elabel] = eclipse.get_flux()
        errors[elabel] = eclipse.get_flux_err()

    # Define the likelihood
    likelihood = JointGaussianLikelihood(freqs, mjds, times, phases, fluxes, 
        errors, parameters=parameters)

    # Run sampling with pymultinest
    results = bilby.run_sampler(priors=priors, likelihood=likelihood, 
        sampler="pymultinest", nlive=1024, evidence_tolerance=0.1,
        importance_nested_sampling=True, resume=True, 
        outdir="{0}/{1}".format(outdir, label))

    # Plot posterior samples with Breton+2008 results overlaid
    injected_params = dict(delta_phi_B=None, theta=130.02, chi=70.94, phi=None,
        r_min=None, upsilon=2.0, z=-0.543, ratio=1.28, sigma_1=None, sigma_2=None)
        #P0_B=2.77346) 

    results.plot_corner(truth=injected_params, priors=True, plot_contours=True,
        dpi=150)
    plt.close()

    Finish = runtime.time()
    print("Parameter estimation took {0} seconds to complete".format(Finish - Start))

    quit()
