import numpy as np
import argparse
import bilby
import time as runtime
import matplotlib.pyplot as plt

from astropy.time import Time
from likelihood import TailoredGaussianLikelihood
from template import generate_eclipse

# Global params
SEC_PER_DAY = 24*60*60
DEG_TO_RAD = np.pi/180

def get_input_arguments(parser):
    """
    Get the input arguments specified by the user.
    """
    parser.add_argument("-e", dest="eclipse_file", type=str,
        help="Input .eclipse file.")
    parser.add_argument("-b", dest="band", type=str,
        help="Frequency band [Lband, UHF]")
    parser.add_argument("-l", dest="label", type=str, help="Output label")
    
    return parser.parse_args()


def compute_B_period(MJD):
    return 2.77346077007 + (8.92e-16 * (MJD - 53156.0))


def predict_phi(t_1):
    phi_0 = 51.20
    t_0 = 53857.0
    return phi_0 - 4.76*((t_1 - t_0)/365.25)


class eclipse_data():
    """
    Unpack data from an input eclipse textfile.
    """
    def __init__(self, filename):

        self.datafile = np.genfromtxt(filename, skip_header=4,
            delimiter=" ")
        self.orbital_phase = self.datafile[:,1]/360.
        self.on_eclipse = np.transpose(np.argwhere(
            (self.orbital_phase >= 0.247) & (self.orbital_phase <= 0.2517)))[0]

    def get_date_mjd(self):
        return self.datafile[self.on_eclipse,0][0]

    def get_time(self):
        self.date_mjd = self.get_date_mjd()
        return (self.datafile[self.on_eclipse,0] - self.date_mjd) * SEC_PER_DAY

    def get_orbital_phase(self):
        return self.orbital_phase[self.on_eclipse]

    def get_flux(self):
        self.flux = ( self.datafile[self.on_eclipse,2]
            /np.median(self.datafile[:self.on_eclipse[0],2]) )
        return self.flux

    def get_flux_err(self):
        self.flux_err = ( self.datafile[self.on_eclipse,3]
            /np.median(self.datafile[:self.on_eclipse[0],2]) )
        return self.flux_err


# If run directly
if __name__ == "__main__":
    Start = runtime.time()

    #outdir = "/fred/oz005/users/mlower/double_psr/double_psr_eclipses/pipeline/outdir"
    outdir = "./outdir/singles"

    parser = argparse.ArgumentParser()
    args = get_input_arguments(parser)

    label_split_1 = args.label.split("-")
    label_split_2 = label_split_1[3].split(":")
    
    obs_label = "eclipse_{0}{1}{2}_{3}{4}{5}".format(label_split_1[0], 
        label_split_1[1], label_split_1[2], label_split_2[0], label_split_2[1], 
        label_split_2[2])

    if args.band == "Lband":
        freq = 1.28446
    elif args.band == "UHF":
        freq = 0.81573
    elif args.band == "GBT820":
        freq = 0.82000
    else:
        raise ValueError("Frequency band (currently) unsupported.")

    eclipse = eclipse_data(args.eclipse_file)

    # Get spin period of B at eclipse date
    P0_B = compute_B_period(eclipse.get_date_mjd())

    # Predict mean phi from Breton+2008
    phi_pred = predict_phi(eclipse.get_date_mjd())
    print(r"Predited $\phi$({0}) = {1}".format(eclipse.get_date_mjd(), phi_pred))

    # Set-up priors
    parameters = dict(delta_phi_B=None, P0_B=None, theta=None, chi=None, 
        phi=None, r_min=None, upsilon=None, z=None, ratio=None, sigma=None)

    # Restricted Gaussian priors
    priors = dict()
    priors["delta_phi_B"] = bilby.core.prior.Uniform(0, 1, r"$\Delta\phi_{\rm B}$")
    priors["P0_B"] = bilby.core.prior.DeltaFunction(P0_B, r"$P_{\rm B}$")
    priors["theta"] = bilby.core.prior.Gaussian(130, 2, r"$\theta$")
    priors["chi"] = bilby.core.prior.Gaussian(71, 2, r"$\chi$")
    priors["phi"] = bilby.core.prior.Uniform(-90, 90, r"$\phi_{\rm B}$")
    #priors["phi"] = bilby.core.prior.Gaussian(phi_pred, 1, r"$\phi_{\rm B}$")
    priors["r_min"] = bilby.core.prior.Uniform(0, 1, r"$r_{\rm min}$")
    priors["upsilon"] = bilby.core.prior.Gaussian(2, 0.2, r"$\upsilon$")
    priors["z"] = bilby.core.prior.Gaussian(-0.5, 0.5, r"$z_{0}/R_{\rm mag}$")
    priors["ratio"] = bilby.core.prior.Gaussian(1.0, 0.2, r"$\xi$")
    priors["sigma"] = bilby.core.prior.Uniform(0, 2, r"$\sigma_{\rm noise}$")

    # Uniform priors (replace above with these)
    #priors = dict()
    #priors["delta_phi_B"] = bilby.core.prior.Uniform(0, 1, r"$\Delta\phi_{\rm B}$")
    #priors["P0_B"] = bilby.core.prior.Gaussian(2.77346, 0.1, r"$P_{\rm B}$")
    #priors["theta"] = bilby.core.prior.Uniform(90, 180, r"$\theta$")
    #priors["chi"] = bilby.core.prior.Uniform(0, 90, r"$\chi$")
    #priors["phi"] = bilby.core.prior.Uniform(-90, 90, r"$\phi$")
    #priors["r_min"] = bilby.core.prior.Uniform(0, 1, r"$r_{\rm min}$")
    #priors["upsilon"] = bilby.core.prior.Uniform(0, 10, r"$\upsilon$")
    #priors["z"] = bilby.core.prior.Uniform(-2, 2, r"$z_{0}/R_{\rm mag}$")
    #priors["ratio"] = bilby.core.prior.Uniform(0.5, 2.0, r"$\xi$")
    #priors["sigma"] = bilby.core.prior.Uniform(0, 2, r"$\sigma_{\rm noise}$")

    # Define the likelihood
    likelihood = TailoredGaussianLikelihood(time=eclipse.get_time(), 
        orbital_phase=eclipse.get_orbital_phase(), flux=eclipse.get_flux(), 
        flux_err=eclipse.get_flux_err(), date=eclipse.get_date_mjd(), 
        nu=freq, parameters=parameters)

    # Run sampling with dynesty
    """
    results = bilby.run_sampler(priors=priors, likelihood=likelihood, sampler="dynesty", nlive=1024,
        nwalks=None, outdir=args.outdir, label=args.label)
    """

    # Run sampling with pymultinest
    results = bilby.run_sampler(priors=priors, likelihood=likelihood, 
        sampler="pymultinest", walks=100, nlive=1024, evidence_tolerance=0.1,
        importance_nested_sampling=True, resume=True, 
        outdir="{0}/{1}".format(outdir, obs_label))

    # Plot posterior samples with Breton+2008 results overlaid
    injected_params = dict(delta_phi_B=None, P0_B=2.77346, theta=130.02, chi=70.94,
        phi=None, r_min=None, upsilon=2.0, z=-0.543, ratio=1.28, sigma=None) 

    results.plot_corner(truth=injected_params, priors=True, plot_contours=True, dpi=150)
    plt.close()

    Finish = runtime.time()
    print("Parameter estimation took {0} seconds to complete".format(Finish - Start))

    phi_B = results.posterior.delta_phi_B.values
    P_B = results.posterior.P0_B.values

    ToA_offset = (phi_B * P_B) / SEC_PER_DAY

    with open("./outdir/{0}/ToA.tim".format(obs_label), "a") as toa_file:
        toa_file.write("test {0} {1} {2} bat\n".format(
            freq*1e3, eclipse.get_date_mjd() + np.mean(ToA_offset),
            np.std(ToA_offset)))
        toa_file.close()
