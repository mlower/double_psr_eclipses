import numpy as np
import matplotlib.pyplot as plt
import bilby
from template import generate_eclipse 

SEC_PER_DAY = 24*3600

def spin_precession(t_1, t_0, phi_0, Omega_SO):
    return phi_0 - Omega_SO*((t_1 - t_0)/365.25)


class TailoredGaussianLikelihood(bilby.likelihood.Likelihood):
    def __init__(self, time, orbital_phase, flux, flux_err, date, nu, parameters):
        """
        Tailored Gaussian Likelihood
        
        A modified version of the Gaussian likelihood tailored for use on
        Double Pulsar eclipse data.
        
        Parameters
        ----------
        orbital_phase : ndarray
            Orbital phase corresponding to each point in the lightcurve.
        flux : ndarray, None
            Flux for each point in the lightcurve.
            If not provided, the array will be filled with zeros, which can be
            useful for testing and theoretical lightcurve calculations.
        flux_err : ndarray, None
            Uncertainties on the flux at each point in the lightcurve.
            If not provided, the array will be filled with zeros.
        date : str
            The date the observation was taken in ISO format.
        nu : float
            Radio frequency of the observation (GHz).
        parameters : dict_like
            Dictionary of parameter keys.
        """        
        super().__init__()
        self.time = time
        self.orbital_phase = orbital_phase
        self.flux = flux
        self.flux_err = flux_err
        self.date = date
        self.nu = nu
        self.parameters = parameters
        
    def log_likelihood(self):
        self.sigma = np.sqrt((self.parameters["sigma"]**2) + self.flux_err**2) 

        self.model = generate_eclipse(self.time, self.orbital_phase, self.flux,
            self.date, self.nu, **self.parameters)
        self.residual = (self.flux - self.model)**2
        
        ln_like = np.sum(- (self.residual / (2 * self.sigma**2)) 
            - np.log(2 * np.pi * self.sigma**2) / 2)
        
        return ln_like


class PairedGaussianLikelihood(bilby.likelihood.Likelihood):
    def __init__(self, time, orbital_phase, flux, flux_err, date, nu, parameters):
        """
        Paired Gaussian Likelihood

        A modified version of the Gaussian likelihood tailored for use on
        pairs of Double Pulsar eclipse observations.

        Parameters
        ----------
        orbital_phase : ndarray
            Orbital phase corresponding to each point in the lightcurve.
        flux : ndarray, None
            Flux for each point in the lightcurve.
            If not provided, the array will be filled with zeros, which can be
            useful for testing and theoretical lightcurve calculations.
        flux_err : ndarray, None
            Uncertainties on the flux at each point in the lightcurve.
            If not provided, the array will be filled with zeros.
        date : str
            The date the observation was taken in ISO format.
        nu : float
            Radio frequency of the observation (GHz).
        parameters : dict_like
            Dictionary of parameter keys.
        """
        super().__init__()
        self.time = time
        self.orbital_phase = orbital_phase
        self.flux = flux
        self.flux_err = flux_err
        self.date = date
        self.nu = nu
        self.parameters = parameters

    def log_likelihood(self):

        self.dorb = self.parameters["dorb"]

        self.dt = (self.date[1] - self.date[0]) * SEC_PER_DAY

        self.sigma1 = np.sqrt((self.parameters["sigma_1"]**2)
            + self.flux_err[0]**2)
        self.sigma2 = np.sqrt((self.parameters["sigma_2"]**2)
            + self.flux_err[1]**2)

        self.model1 = generate_eclipse(self.time[0], self.orbital_phase[0]-self.dorb, 
            self.flux[0], self.date[0], self.nu, **self.parameters)
        self.residual1 = (self.flux[0] - self.model1)**2

        self.model2 = generate_eclipse(self.time[1]+self.dt, 
            self.orbital_phase[1]-self.dorb, self.flux[1], self.date[1],
            self.nu, **self.parameters)

        self.residual2 = (self.flux[1] - self.model2)**2

        ln_like1 = np.sum(- (self.residual1 / (2 * self.sigma1**2))
            - np.log(2 * np.pi * self.sigma1**2) / 2)

        ln_like2 = np.sum(- (self.residual2 / (2 * self.sigma2**2))
            - np.log(2 * np.pi * self.sigma2**2) / 2)

        ln_like_tot = ln_like1 + ln_like2

        return ln_like_tot


class Paired2DGaussianLikelihood(bilby.likelihood.Likelihood):
    def __init__(self, time, orbital_phase, flux, flux_err, date, nu, parameters):
        """
        Paired Gaussian Likelihood

        A modified version of the Gaussian likelihood tailored for use on
        pairs of Double Pulsar eclipse observations.

        Parameters
        ----------
        orbital_phase : ndarray
            Orbital phase corresponding to each point in the lightcurve.
        flux : ndarray, None
            Flux for each point in the lightcurve.
            If not provided, the array will be filled with zeros, which can be
            useful for testing and theoretical lightcurve calculations.
        flux_err : ndarray, None
            Uncertainties on the flux at each point in the lightcurve.
            If not provided, the array will be filled with zeros.
        date : str
            The date the observation was taken in ISO format.
        nu : float
            Radio frequency of the observation (GHz).
        parameters : dict_like
            Dictionary of parameter keys.
        """
        super().__init__()
        self.time = time
        self.orbital_phase = orbital_phase
        self.flux = flux
        self.flux_err = flux_err
        self.date = date
        self.nu = nu
        self.parameters = parameters

    def log_likelihood(self):

        self.dorb = self.parameters["dorb"]
        self.dorb2 = self.parameters["dorb2"]

        if self.nu == 0.815734375:
            self.freqs = np.linspace(0.559734375, 1.071734375, 4)
        elif self.nu == 0.128358203:
            self.freqs = np.linspace(0.855582031, 1.711582031, 4)
        else:
            raise ValueError("Frequency band (currently) unsupported.")

        self.dt = (self.date[1] - self.date[0]) * SEC_PER_DAY

        self.sigma1 = np.sqrt((self.parameters["sigma_1"]**2)
            + self.flux_err[0]**2)
        self.sigma2 = np.sqrt((self.parameters["sigma_2"]**2)
            + self.flux_err[1]**2)

        self.model1 = np.zeros((len(self.freqs),len(self.time[0])))
        for i in range(0, len(self.freqs)):
            self.model1[i,:] = generate_eclipse(self.time[0],
                self.orbital_phase[0]+self.dorb, self.flux[0], self.date[0], 
                self.freqs[i], **self.parameters)
        self.model1 = np.mean(self.model1, axis=0)
        self.residual1 = (self.flux[0] - self.model1)**2

        self.model2 = np.zeros((len(self.freqs),len(self.time[1])))
        for i in range(0, len(self.freqs)):
            self.model2[i,:] = generate_eclipse(self.time[1]+self.dt, 
                self.orbital_phase[1]+self.dorb+self.dorb2, self.flux[1], self.date[1], 
                self.freqs[i], **self.parameters)
        self.model2 = np.mean(self.model2, axis=0)
        self.residual2 = (self.flux[1] - self.model2)**2

        ln_like1 = np.sum(- (self.residual1 / (2 * self.sigma1**2))
            - np.log(2 * np.pi * self.sigma1**2) / 2)

        ln_like2 = np.sum(- (self.residual2 / (2 * self.sigma2**2))
            - np.log(2 * np.pi * self.sigma2**2) / 2)

        ln_like_tot = ln_like1 + ln_like2

        #print(ln_like_tot)

        return ln_like_tot


class RePairedGaussianLikelihood(bilby.likelihood.Likelihood):
    def __init__(self, time, orbital_phase, flux, flux_err, date, nu, parameters):
        """
        Paired Gaussian Likelihood

        A modified version of the Gaussian likelihood tailored for use on
        pairs of Double Pulsar eclipse observations.

        Parameters
        ----------
        orbital_phase : ndarray
            Orbital phase corresponding to each point in the lightcurve.
        flux : ndarray, None
            Flux for each point in the lightcurve.
            If not provided, the array will be filled with zeros, which can be
            useful for testing and theoretical lightcurve calculations.
        flux_err : ndarray, None
            Uncertainties on the flux at each point in the lightcurve.
            If not provided, the array will be filled with zeros.
        date : str
            The date the observation was taken in ISO format.
        nu : float
            Radio frequency of the observation (GHz).
        parameters : dict_like
            Dictionary of parameter keys.
        """
        super().__init__()
        self.time = time
        self.orbital_phase = orbital_phase
        self.flux = flux
        self.flux_err = flux_err
        self.date = date
        self.nu = nu
        self.parameters = parameters

    def log_likelihood(self):

        self.dorb1 = self.parameters["dorb_1"]
        self.dorb2 = self.parameters["dorb_2"]

        if self.nu == 0.815734375:
            self.freqs = np.linspace(0.559734375, 1.071734375, 4)
        elif self.nu == 0.128358203:
            self.freqs = np.linspace(0.855582031, 1.711582031, 4)
        else:
            raise ValueError("Frequency band (currently) unsupported.")

        self.sigma1 = np.sqrt((self.parameters["sigma_1"]**2)
            + self.flux_err[0]**2)
        self.sigma2 = np.sqrt((self.parameters["sigma_2"]**2)
            + self.flux_err[1]**2)

        self.model1 = np.zeros((len(self.freqs),len(self.time[0])))
        for i in range(0, len(self.freqs)):
            self.parameters["delta_phi_B"] = self.parameters["delta_phi_B_1"]
            self.model1[i,:] = generate_eclipse(self.time[0],
                self.orbital_phase[0]+self.dorb1, self.flux[0], self.date[0],
                self.freqs[i], **self.parameters)
        self.model1 = np.mean(self.model1, axis=0)
        self.residual1 = (self.flux[0] - self.model1)**2

        self.model2 = np.zeros((len(self.freqs),len(self.time[1])))
        for i in range(0, len(self.freqs)):
            self.parameters["delta_phi_B"] = self.parameters["delta_phi_B_2"]
            self.model2[i,:] = generate_eclipse(self.time[1],
                self.orbital_phase[1]+self.dorb2, self.flux[1], self.date[1],
                self.freqs[i], **self.parameters)
        self.model2 = np.mean(self.model2, axis=0)
        self.residual2 = (self.flux[1] - self.model2)**2

        ln_like1 = np.sum(- (self.residual1 / (2 * self.sigma1**2))
            - np.log(2 * np.pi * self.sigma1**2) / 2)

        ln_like2 = np.sum(- (self.residual2 / (2 * self.sigma2**2))
            - np.log(2 * np.pi * self.sigma2**2) / 2)

        ln_like_tot = ln_like1 + ln_like2

        #print(ln_like_tot)

        return ln_like_tot


class SimplifiedLikelihood(bilby.likelihood.Likelihood):
    def __init__(self, eclipse_1, eclipse_2, nu, parameters):
        """
        Paired Gaussian Likelihood

        A modified version of the Gaussian likelihood tailored for use on
        pairs of Double Pulsar eclipse observations.

        Parameters
        ----------
        orbital_phase : ndarray
            Orbital phase corresponding to each point in the lightcurve.
        flux : ndarray, None
            Flux for each point in the lightcurve.
            If not provided, the array will be filled with zeros, which can be
            useful for testing and theoretical lightcurve calculations.
        flux_err : ndarray, None
            Uncertainties on the flux at each point in the lightcurve.
            If not provided, the array will be filled with zeros.
        date : str
            The date the observation was taken in ISO format.
        nu : float
            Radio frequency of the observation (GHz).
        parameters : dict_like
            Dictionary of parameter keys.
        """
        super().__init__()
        self.eclipse_1 = eclipse_1
        self.eclipse_2 = eclipse_2
        self.nu = nu
        self.parameters = parameters

    def log_likelihood(self):

        self.dorb1 = self.parameters["dorb_1"]
        self.dorb2 = self.parameters["dorb_2"]

        # Getting appropriate frequencies for each subband
        if self.nu == 0.815734375:
            self.freqs = np.linspace(0.559734375, 1.071734375, 4)
        elif self.nu == 0.128358203:
            self.freqs = np.linspace(0.855582031, 1.711582031, 4)
        else:
            raise ValueError("Frequency band (currently) unsupported.")

        # Weighting the uncertainties
        self.sigma_1 = np.sqrt((self.parameters["sigma_1"]**2)
            + self.eclipse_1.get_flux_err()**2)
        self.sigma_2 = np.sqrt((self.parameters["sigma_2"]**2)
            + self.eclipse_2.get_flux_err()**2)

        # Generate subbanded models for eclipse no. 1
        self.model_1 = np.zeros((len(self.freqs),len(self.eclipse_1.get_orbital_phase())))
        for i in range(0, len(self.freqs)):
            self.parameters["delta_phi_B"] = self.parameters["delta_phi_B_1"]

            self.model_1[i,:] = generate_eclipse(
                self.eclipse_1.get_time(),
                self.eclipse_1.get_orbital_phase() + self.dorb1,
                self.eclipse_1.get_flux(),
                self.eclipse_1.get_date_mjd(),
                self.freqs[i], 
                **self.parameters
            )
        # Average subbands together and then subtract template from the eclipse 1
        self.model_1 = np.mean(self.model_1, axis=0)
        self.residual_1 = (self.eclipse_1.get_flux() - self.model_1)**2

        # Generate subbanded models for eclipse no. 2
        self.model_2 = np.zeros((len(self.freqs),len(self.eclipse_2.get_orbital_phase())))
        for i in range(0, len(self.freqs)):
            self.parameters["delta_phi_B"] = self.parameters["delta_phi_B_2"]

            self.model_2[i,:] = generate_eclipse(
                self.eclipse_2.get_time(),
                self.eclipse_2.get_orbital_phase() + self.dorb2,
                self.eclipse_2.get_flux(),
                self.eclipse_2.get_date_mjd(),
                self.freqs[i], 
                **self.parameters
            )
        # Average subbands together and then subtract template from the eclipse 2
        self.model_2 = np.mean(self.model_2, axis=0)
        self.residual_2 = (self.eclipse_2.get_flux() - self.model_2)**2

        # Compute likleihoods separately, then combine
        ln_like1 = np.sum(- (self.residual_1 / (2 * self.sigma_1**2))
            - np.log(2 * np.pi * self.sigma_1**2) / 2)

        ln_like2 = np.sum(- (self.residual_2 / (2 * self.sigma_2**2))
            - np.log(2 * np.pi * self.sigma_2**2) / 2)

        ln_like_tot = ln_like1 + ln_like2

        #print(ln_like_tot)
        #quit()

        return ln_like_tot
